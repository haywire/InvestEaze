user = {
 _id
 authkey : {
   twitterid
   facebookid
   LinkedinId
 },
 personalDetails: {
   default: linkedIn
   name
   dob
   location
   username
   email
   password
   address: {name, street, city, state, country, phone}
   imageLink  
 },
 profile_Links: {
   fb
   tw
   li
 },
 achievements: {
   accredited: bool
   badges: [
   {id, text},
   {id, text}...
   ]
 },
 tags: [
 {id, text},
 {id, text}...
 ],
 logDetails: {
   email_activated: bool,
   last_logged: date/time,
   user_hash
 },
 proDetails: {
   title: [
   {
     text
     company
   }  
   ],
   previousCompanies: [
   {"name":"Goldmann Sachs"}...
   ]
 }
 followers: [
 {id}...
 ],
 following: [
 {id}...  
 ],   
}

post = {
 _id,
 initiator,
 time,
 content : {
   image, hyperlink, title, content
 }
 type: post/idea    
 nooflikes : 
 noofcomments :
 noofshares :
 sharedfrom : 
}

comment = {
  _id
  initiator
  time
   content : {
     image, hyperlink, title, content
   }
   parentID
   parentIdInitiator,
   nooflikes : 
}

Like / Share / Invested
activity = {
  _id,
  parentId,
  parentIdInitiator,
  parentCollection,
  type: {liked/invested/shared/followed}
  initiator
  time
}

notification - {
  _id,
  userId,
  collection,  // post, coment, activity
  collection_id

}

message = {
  _id,
  from,
  to: [userId, raed/unread/archived],
  content
  attachment: []
  time
}

fundHouseProfile = {
  _id,
  owner: []
  description
  strategy: [
    {...}...
  ]
    persoanl: {
      name
        founded
        location
        email
        address: {name, street, city, state, country, phone}
        logoImage
    }
    profile_Links: {
      fb
      tw
      li
    },
  team: [
  {
    user._id, user.personalDetails, user.profileLinks
  }...
  ],
  reviews:[
   {
    from: user._id, user.personalDetails
    content
    date
    starRating: int
  }
  ]
  tags: [
    {id, text}...
  ]
  qna: [
    {qtext, atext}
  ]
  pitch: [
    {links}
  ]
  followers: [
    {id}...
  ]
  aum: int  
}

fund = {
  id,
  fh_id,
  description,
  persoanl= {
    name
        founded
        location
        email
        address: {name, street, city, state, country, phone}
        logoImage
  }
  team: [
  {
    user._id, user.personalDetails, user.profileLinks
  }...
  tags: [
    {id, text}...
  ]
  pitch: [
    {links}
  ]
  strategy: [
    {...}...
  ]
  qna: [
    {qtext, atext}
  ]
  raiseDetails: {
    startDate
    endDate
    amountRaising
    raisedTillNow
  }
    terms: [
    {}...
    ]
    followes: [
    {users}
    ]
    refer: [
    {
      initiator
      initiated
    }...
    ]
}

news = {
id
link
type: news/newsletter/filing
date
fundId/fhId
ftype: fund/fundHouse
}


fund_user_mapping: [
{_id
  fundId
  userId
  amount
  units_purchased
  date
  closed=bool
  closedDate
}
]

fund_metrics : [
  _id
  fund_id
  price= [ date, price]
  sharpe ratio [ date, price]
  yearlyalpha [ date, price]
  3mreturn [ date, price]
  6mreturn [ date, price]
]