/**
 * FundUserMapping
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
  	
  	fundId : {
        type: "string",
        required: true
    },
    userId : {
        type: "string",
        required: true
    },
    amount : {
        type: "float",
        required: true
    },
    units_purchased : {
        type: "integer"
    },
    date : {
    	type: "date" 
    },
    isClosed : {
    	type: "boolean",
    	required: true
    },
    closedDate : {
    	type: "date" 
    },

  }
};