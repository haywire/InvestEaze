
module.exports = {

    attributes: {

        parentId : {
            type : 'string',
            required : true
        },
    
        // id of the person who initiated the parent commment or the post
        // helps in notifications
        parentIdInitiator : {
            type : 'string'
        },

        parentCollection : {
            type : 'string', // Fund/FundHouse/Post/Comment/User
            required : true
        },

        initiator : {
            type : 'string',
            required : true
        },
        
        // If the activity is of type Refer, then initiated is the person/user
        // to whom a fund/fund house is referred
        initiated : {
            type : 'string' 
        },
        
        // If the activity is of type Invest, then amount represents the amount
        // of capital invested by the invester in the corresponding fund.
        amount : {
            type : 'float' 
        },

        // If a person A, posts a post P, which is eventually a shared post.
        // and if P was shared from original post O, then O'd id is the originalPostId  
        sharedPostId : {
            type : 'string' 
        },

        activityType : {
            type : 'string', // {Like/Unlike/Invest/Share/Follow/Unfollow/Refer}
            required : true
        }, 
    }
}

/*
*  Post P of user A is  shared by user B which generates a new post O,
*   P : parentId
*   A : parentIdInitiator
*   B : initiator
*   O : sharedPostId 
*/

