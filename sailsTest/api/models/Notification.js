module.exports = {

    attributes: {

        // The user for who is supposed to be notified
        userId : {
            type : 'string',
            required : true
        },
        collection : {
        	type : 'string',  // post, comment, activity
        	required : true
        },

        // Id of the post/comment/activity in their respective collections
        collectionId : {
        	type : 'string',
        	required : true
        },

        // If a person A, posts a post P, which is eventually a shared post.
        // and if P was shared from original post O, then O'd id is the originalPostId  
        sharedPostId : {
        	type : 'string'
        },

        // If the notification has been read or not
        isRead : {
            type : 'boolean',
            required : true,
            defaultsTo : false
        }
    }
};

/*
*  Post P of user A is  shared by user B which generates a new post O,
*   P : parentId
*   A : parentIdInitiator
*   B : initiator
*   O : sharedPostId 
*/