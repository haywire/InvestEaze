module.exports = {
    
    attributes: {

        // Id of the fund in User Model
        fundId : {
            type: "string",
            required: true
        },
        fundType : {
            type: "string"
        },
        investmentPlan : {
            type: "string"
        },
        benchMark : {
            type: "string"
        },
        launchDate : {
            type: "date",
            required: true
        },
        assetSize : {
            type: "float"
        },
        minimumInvestment : {
            type: "integer"
        },
        lastDividend : {
            type: "float"
        },
        bonus : {
            type: "float"
        },
        fundManager : {
            type: "string"
        },
        notes : {
            type: "string"
        },

        // Load Details

        entryLoad : {
            type: "float"
        },
        exitLoad : {
            type: "float"
        },
        loadComments : {
            type: "string"
        },


        // Contact Details

        office : {
            type: "text"
        },
        telephoneNumber : {
            type: "string"
        },
        faxNumber : {
            type: "string"
        },
        email : {
            type: "string",
            email: true
        },
        website : {
            type: "string",
            url: true
        }
    }
};