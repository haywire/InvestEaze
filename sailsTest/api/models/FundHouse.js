/**
 * FundHouseProfile
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {

  	owner : {
        type: "array",
        required: true
    },
    description : {
    	type: "text"
    },
    // array of string values ["value1","value2", ..]
    strategy : {
    	type: "array"
    },
    name : {
    	type: "string"
    },
    date : {
    	type: "date"
    },
    location : {
    	type: "string"
    },
    email: {
    	type: 'email',
	    required: true
    },
    addressName : {
    	type: "string"
    },
    street : {
    	type: "string"
    },
    city : {
    	type: "string"
    },
    state : {
    	type: "string"
    },
    country : {
    	type: "string"
    },
    phone : {
    	type: "string"
    },
    logoImage : {
    	type: "string"
    },
    twitterLink : {
    	type: "string"
    },
    fbLink : {
    	type: "string"
    },
    linkedinLink : {
    	type: "string"
    },
    //This is an array of Sub user profile objects: 
    //[{userId:"122233",(UserProfile Values),(Usersocial profile links)}, {...]
    team : {
    	type: "array",
      defaultsTo : []
    },
    // This is an aray of userDetails who reviewed, starRating, content and review date
    // [{userId:"122233",(UserProfile Values),(Usersocial rpofile links), content: "review content", 
    // "starRating":4, date:"Linux Date format" }, {...]
    reviews : {
    	type: "array" 
    },
    // array for tagId, and Tag Text [{tagId:"2w21w", tagText:"TagValue"},{..]
    tags : {
    	type: "array",
      defaultsTo : []
    },
    // array for Questions and answers [{qText:"What is a Fund House?", aText:"TIt is a house of funds"},{..]
    qna : {
    	type: "array",
      defaultsTo : []
    },

    // array of links to pitch documents ["http://domain.com/dir1/doc", ..]
    pitch : {
    	type: "array"
    },

    // array of user Id's who follow the fund house ["id1", "id2", ..]
    followers : {
    	type: "array",
        defaultsTo : []
    },

    // array of JSON documents with Id's of users who invested, the amount which
    backers : {
        type: "array",
        defaultsTo : []
    }, 
    
    // An array of objects with initiator and initiated
    // [{initiator: "userId", initiated:"userId"}, {...]
    referrals : {
        type: "array",
        defaultsTo : []
    },
    aum : {
    	type: "integer"
    },
    numberOfLikes : {
        type : 'integer',
        defaultsTo : 0
    },
    isDeleted : {
        type: 'boolean',
        defaultsTo : false
    }
 
  }

 };