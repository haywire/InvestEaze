/**
 * UserController
 *
 */

 // Imports
var passport = require("passport");
var async = require("async");

// ObjectID helps in creating mongodb type id for querying database
var ObjectId = require('mongodb').ObjectID;

module.exports = {

  find : function (req, res) { 

    var id;

    // Check if req is a HTTP request or a JSON request
    if(req.id)
      id = req.id;
    else
      id = req.param('id');

    console.log("Fetching User Details: " + id);

    User.native(
      function(err, collection){
        collection.findOne(
          {
            _id: new ObjectId(id)
            //"isDeleted": false
          },
          {
         
            investments: 0,
            password: 0
          },
          function(err, user){
            if(err){
                // User could not be found!
                console.log("Error finding User: " + JSON.stringify(err));
                return res.json(
                    {
                      success: false,
                      message: "Error finding User"
                    },
                    404
                  );
            } else {
              return res.json(
                user,
                200
              );
            }
          }
        )
      }
    );
  },

  findName : function (req,callback) { 

    var id;

    if(req.id) {
      id = req.id;
    } else {
      id = req;
    }

    User.native(
      function(err, collection){
        collection.findOne(
          {
            _id: new ObjectId(id),
            //"isDeleted": false
          },
          {
            name : 1,
            imageLink: 1
          },
          function(err, userDetails){
            if(err){
                // User could not be found!
                console.log("Error finding User: " + JSON.stringify(err));
                return res.json(
                    {
                      success: false,
                      message: "Error finding User"
                    },
                    404
                  );
            } else {
              if(userDetails) {
               
                if(req.id){
                  callback(userDetails, req.index);  
                } else {
                  callback(userDetails);
                }
                
              } else {
                callback(userDetails);
              }
            }
          }
        )
      }
    );
  },

    list: function(req, res) {

      console.log("Fetching Users " + JSON.stringify(req.body));

      var request = req.body;

      /*
      * Paginating the users list using page number and 
      * number of documents per page. Combined aprroach - range by "_id" + skip()
      * Using skip+limit is not a good way to do paging when performance is an issue,
      * or with large collections; it will get slower and slower as you increase the page number.
      * Using skip requires the server to walk though all the documents (or index values) from 0 
      * to the offset (skip) value. It is much better to use a range query (+ limit) where you pass
      * in the last page's range value.
      */

      var pageSize = req.param("size");
      var currentPageNumber = req.param("pageFrom");
      var newPageNumber = req.param("pageTo");
      var currentId = req.param("currentId"); // id of the first record on the current page
      var pageDiff = currentPageNumber - newPageNumber;
      var skip;

      // Loading Deafults
      if(!currentId)
        currentId = 0;
      if(!pageSize)
        pageSize = 10;
      if(!pageDiff)
        skip = 0;

      // Calculating the number of documents to skip and the queryParameter
      var queryParameter = {};
      if(pageDiff < 0) {

        if(!skip)
          skip = pageSize * ( ( (-1) * pageDiff) - 1);

        queryParameter = {
                          _id : 
                            {
                              $lt: new ObjectId(currentId)/*,
                              "isDeleted": false*/
                            }
                        }
      
      } else {
      
        if(!skip)
          skip = pageSize * pageDiff;
        
        queryParameter = {
                          _id : 
                            {
                              $gt: new ObjectId(currentId)/*,
                              "isDeleted": false*/
                            }
                        }
      }

      // Fetching Users List
      User.native(
        function(err,collection){
          if(err){
            console.log("Error making native query: " + JSON.stringify(err));
            return res.json(
              {
                success: false,
                message: "Error making native query"
              }
            );
          } else {
              collection.find(
                queryParameter,
                {
                  name: 1,
                  username: 1,
                  email: 1,
                  imageLink: 1
                }
              )
              .skip(skip)
              .limit(parseInt(pageSize))
              .sort({_id: 1}) // Sort by Id
              .toArray(
                function(err, users){
                  if(err){
                    // Users could not be fetched!
                    console.log("Error Fetching Users: "+ JSON.stringify(err));
                    return res.json(
                      {
                        success: false,
                        message: "Error Fetching Users"
                      },
                      404
                    );
                  } else {
                    return res.json(
                        users,
                        200
                    );
                  }
                }
              );
            }
          }
        );
    },
    update : function (req, res) { 
      console.log("Updating User Details: " + JSON.stringify(req.body));

      var id = req.param('id');
      var updateParameters = req.body;

       User.native(
        function(err, collection){
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $set: updateParameters
            },
            function(err, user){
              if(err){
                  // User could not be updated!
                  console.log("Error updating User: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error updating User"
                      }
                    );
              } else {
                return res.json(
                  {
                    success: true,
                    message: "Success Updating User",
                  }
                );
              }
            }
          )
        }
      );
    },

    destroy : function (req, res) { 
      
      console.log("Soft Deleting User : " + JSON.stringify(req.body));

      var id = req.param('id');

      // Soft deleting user
      User.native(
        function(err, collection){
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $set: {"isDeleted": true}
            },
            function(err, user){
              if(err){
                  // User could not be soft deleted!
                  console.log("Error soft deleting User: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error soft deleting User"
                      }
                    );
              } else {
                return res.json(
                  {
                    success: true,
                    message: "Success soft deleting User",
                  }
                );
              }
            }
          )
        }
      );
    },

    // Add a follower to the user
    addFollower : function (req, res) {
      
      console.log("Adding Follower to User: " + JSON.stringify(req));

      var id = req.id;
      var followerId = req.followerId;

      // Adding followee's Id to the Follower's following.. !

      User.native(
        function(err,collection){
            
            collection.update(
              {
                _id: new ObjectId(followerId)
              },
              {
                $addToSet: { 
                  following: {
                    id: id,
                    collection: "User"
                  }
                }
              },
              function(err,updatedUser){
              if(err){
                // User could not be updated!
                console.log("Error Adding Following to User: " + JSON.stringify(err));
                return res.json(
                    {
                      success: false,
                      message: "Error Adding Following"
                    }
                  );
              }
              else {
                // User was successfully updated!
                console.log("Success Adding Following to User: " + updatedUser);
                return res.json(
                    {
                      success: true,
                      message: "Success Adding Following"
                    }
                  );
              }
            }
            );
          }
        );

        // Adding follower's Id to the Followee's follower.. This is soo confusing!
        User.native(
        function(err,collection){
          
            collection.update(
              {
                _id: new ObjectId(id)
              },
              {
                $addToSet: { 
                  followers: followerId
                }
              },
              function(err,updatedUser){
              if(err){
                // User could not be updated!
                console.log("Error Adding Follower to User: "+ JSON.stringify(err));
                return res.json(
                    {
                      success: false,
                      message: "Error Adding Follower"
                    }
                  );
              }
              else {
                // User was successfully updated!
                console.log("Success Adding Follower to User: " + updatedUser);
                return res.json(
                    {
                      success: true,
                      message: "Success Adding Follower"
                    }
                  );
              }
            }
            );
          }
        );
    },
    removeFollower : function (req, res) {
      
      console.log("Removing Follower from User: " + JSON.stringify(req));

      var id = req.id;
      var followerId = req.followerId;

      // Removing followee's Id to the Follower's following.. This is soo confusing!

      User.native(
        function(err,collection){
          
            collection.update(
              {
                _id: new ObjectId(followerId)
              },
              {
                $pull: { 
                  following: {
                    id: id,
                    collection: "User"
                  }
                }
              },
              function(err,updatedUser){
              if(err){
                // User could not be updated!
                console.log("Error Removing Following from User: "+ JSON.stringify(err));
                return res.json(
                    {
                      success: false,
                      message: "Error Removing Following"
                    }
                  );
              }
              else {
                // User was successfully updated!
                console.log("Success Removing Following from User: " + updatedUser);
                return res.json(
                    {
                      success: true,
                      message: "Success Removing Following"
                    }
                  );
              }
            }
            );
          }
        );

        // Removing follower's Id to the Followee's follower.. This is soo confusing!
        User.native(
        function(err,collection){
          
            collection.update(
              {
                _id: new ObjectId(id)
              },
              {
                $pull: { 
                  followers: followerId
                }
              },
              function(err,updatedUser){
              if(err){
                // User could not be updated!
                console.log("Error Removing Follower from User: "+ JSON.stringify(err));
                return res.json(
                    {
                      success: false,
                      message: "Error Removing Follower"
                    }
                  );
              }
              else {
                // User was successfully updated!
                console.log("Success Removing Follower from User: " + updatedUser);
                return res.json(
                    {
                      success: true,
                      message: "Success Removing Follower"
                    }
                  );
              }
            }
            );
          }
        );
    },
    // Add an investement to the user
    addInvestment : function (req, res) {
      
      console.log("Adding Investment to User: " + req);

      var id = req.id;

      // Adding followee's Id to the Follower's following.. !

      User.native(
        function(err,collection){
            
            collection.update(
              {
                _id: new ObjectId(id)
              },
              {
                $addToSet: { 
                  investments: {
                    fundId: req.fundId,
                    amount: req.amount,
                    date: new Date()
                  }
                }
              },
              function(err,updatedUser){
              if(err){
                // User could not be updated!
                console.log("Error Adding Investment to User: " + JSON.stringify(err));
                return res.json(
                    {
                      success: false,
                      message: "Error Adding Investment"
                    }
                  );
              }
              else {
                // User was successfully updated!
                console.log("Success Adding Investment to User: " + updatedUser);
                return res.json(
                    {
                      success: true,
                      message: "Success Adding Investment"
                    }
                  );
              }
            }
            );
          }
        );
    },
    fetchInvestments : function (req, res) {

      // Following is the mongoDB query for pagination of embedded arrays using $slice operator
      //db.user.find({_id: ObjectId("537b55a2d38701d0ef685019")},{follower: {$slice:[0, 3]}, following:0, password:0})
      var id = req.param("id");

      console.log("Fetching Investments for User: " + id);

      var pageSize;
      var pageNumber;

      // Paginating the followers list using page number and 
      // number of documents per page
      if(req.param("size"))
        pageSize = req.param("size");
      else
        pageSize = 10;
     
      if(req.param("page"))
        pageNumber = req.param("page");
      else
        pageNumber = 1;

      // Calculating the number of documents to skip
      var skip = pageSize * (pageNumber - 1);

      User.native(
        function(err, collection){
          collection.findOne(
            {
              _id: new ObjectId(id)
            },
            {
              investments: {
                $slice:
                  [
                    skip,
                    parseInt(pageSize)
                  ]
              },
              _id: 0,
              following: 0,
              followers: 0,
              password: 0
            },
            function(err, user){
              if(err){
                  // User could not be found!
                  console.log("Error finding User: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding User"
                      }
                    );
              } else {

                var counter = 0;
                var investments = user.investments;

                for(var i=0; i < investments.length; i++){
                  sails.controllers.user.findName(
                    investments[i].initiator,
                    function(name){
                      
                      investments[counter].initiator_name = name;
                      counter++;

                      if(counter == investments.length){
                        return res.json( 
                          investments,
                            200
                        );
                      }
                    }
                  )
                }
              }
            }
          )
        }
      );

    },
    fetchFollowers : function (req, res) {

      // Following is the mongoDB query for pagination of embedded arrays using $slice operator
      //db.user.find({_id: ObjectId("537b55a2d38701d0ef685019")},{follower: {$slice:[0, 3]}, following:0, password:0})
      var request = req.body;
      var id = req.param("id");

      // Paginating the followers list using page number and 
      // number of documents per page
      var pageSize = req.param("size");
      var page = pageSize;
      var pageNumber = req.param("page");

      // Calculating the number of documents to skip
      var skip = pageSize * (pageNumber - 1);

      User.native(
        function(err,collection){
          
           collection.findOne(
              {
                _id: new ObjectId(id)
              },
              {
                followers: {
                  $slice:
                    [
                      skip, 
                      parseInt(pageSize)
                    ]
                },
                _id:0,
                following:0,
                investments:0,
                password:0
              },
              function(err, user){

                if(err){
                  // User could not be found!
                  console.log("Error finding User: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding User"
                      }
                    );
                } else{

                  // User found!
                  console.log("Success finding User: " + JSON.stringify(user));
                 
                  var followerIds = [];
                  for(var i=0; i < user.followers.length; i++){
                    followerIds.push(new ObjectId(user.followers[i]));
                  }

                  User.native(
                    function(err,collection){
                      
                      collection.find(
                        {
                          _id: { "$in" : followerIds }
                        },
                        {
                          name: 1,
                          imageLink:1
                        }
                      )
                      .toArray(function(err, followers){
                        
                        if(err){
                          // Followers could not be fetched
                          console.log("Error Fetching Followers: "+ JSON.stringify(err));
                          return res.json(
                              {
                                success: false,
                                message: "Error Fetching Followers"
                              }
                            );
                        } else {
                            return res.json(
                            {
                              success: true,
                              message: "Success Fetching Followers",
                              followers: followers
                            }
                          );
                        }
                      });
                    }
                  );

                }
              }
            )
        }
      );
    },
    fetchFollowing : function (req, res) {

      var request = req.body;
      var id = req.param("id");

      // Paginating the followers list using page number and 
      // number of documents per page
      var pageSize = req.param("size");
      var page = pageSize;
      var pageNumber = req.param("page");

      // Calculating the number of documents to skip
      var skip = pageSize * (pageNumber - 1); 

      User.native(
        function(err,collection){
          if(err){
            console.log("Error making native query: "+JSON.stringify(err));
            return res.json(
                      {
                        success: false,
                        message: "Error making native query"
                      }
                    );
          }
          else{
            collection.findOne(
              {
                _id: new ObjectId(id)
              },
              {
                following: {
                  $slice:
                    [
                      skip, 
                      parseInt(pageSize)
                    ]
                },
                _id:0,
                followers:0,
                investments:0,
                password:0
              },
              function(err, user){
                if(err){
                  // User could not be found!
                  console.log("Error finding User: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding User"
                      }
                    );
                } else {
                  console.log("User: "+JSON.stringify(user));

                  // Initializing arrays of Users, Funds and FundHouses that are 
                  // followed by the user. 

                  var usersFollowed = [];
                  var fundsFollowed = [];
                  var fundHousesFollowed = [];

                  // Iterating over the following array and adding the entity 
                  // followed to the respective array
                  for(var i=0; i < user.following.length; i++){

                    var currentFollowing = user.following[i];

                    if(currentFollowing.collection == 'User')
                      usersFollowed.push(new ObjectId(currentFollowing.id));
                    
                    else if(currentFollowing.collection == 'Fund')
                      fundsFollowed.push(new ObjectId(currentFollowing.id));
                    
                    else if(currentFollowing.collection == 'FundHouse')
                      fundHousesFollowed.push(new ObjectId(currentFollowing.id));
                  }

                  // Fetching User,Fund and FundHouse entities details parallelly,
                  // and returning them to the callback
                  async.parallel(
                    [
                      function(callback){
                        // Fetching Users being followed
                        User.native(
                          function(err,collection){
                            if(err){
                              console.log("Error making native query: "+JSON.stringify(err));
                              return res.json(
                                {
                                  success: false,
                                  message: "Error making native query"
                                }
                              );
                            } else {
                              collection.find(
                                {
                                  _id: { "$in" : usersFollowed }
                                },
                                {
                                  name: 1,
                                  imageLink:1
                                }
                              )
                              .toArray(
                                function(err, users){
                                  if(err){
                                    // Following could not be fetched!
                                    console.log("Error Fetching Following: "+ JSON.stringify(err));
                                    return res.json(
                                        {
                                          success: false,
                                          message: "Error Fetching Following"
                                        }
                                      );
                                  } else {
                                      callback(null,users);
                                  }
                                }
                              );
                            }
                          }
                        );
                      },
                      function(callback){
                        // Fetching Funds being followed
                        Fund.native(
                          function(err,collection){
                            if(err){
                              console.log("Error making native query: "+JSON.stringify(err));
                              return res.json(
                                {
                                  success: false,
                                  message: "Error making native query"
                                }
                              );
                            } else {
                              collection.find(
                                {
                                  _id: { "$in" : fundsFollowed }
                                },
                                {
                                  name: 1,
                                  logoImage:1
                                }
                              )
                              .toArray(
                                function(err, funds){
                                  if(err){
                                    // Following could not be fetched!
                                    console.log("Error Fetching Following: "+ JSON.stringify(err));
                                    return res.json(
                                        {
                                          success: false,
                                          message: "Error Fetching Following"
                                        }
                                      );
                                  } else {
                                      callback(null,funds);
                                  }
                                }
                              );
                            }
                          }
                        );
                      },
                      function(callback){
                          // Fetching FundHouses being followed
                          FundHouse.native(
                            function(err,collection){
                              if(err){
                                console.log("Error making native query: "+JSON.stringify(err));
                                return res.json(
                                  {
                                    success: false,
                                    message: "Error making native query"
                                  }
                                );
                              } else {
                                collection.find(
                                  {
                                    _id: { "$in" : fundHousesFollowed }
                                  },
                                  {
                                    name: 1,
                                    logoImage:1
                                  }
                                )
                                .toArray(
                                  function(err, fundHouses){
                                    if(err){
                                      // Following could not be fetched!
                                      console.log("Error Fetching Following: "+ JSON.stringify(err));
                                      return res.json(
                                        {
                                          success: false,
                                          message: "Error Fetching Following"
                                        }
                                      );
                                    } else {
                                      callback(null,fundHouses);
                                    }
                                  }
                                );
                              }
                            }
                          );
                      }
                      
                  ],
                  function(err, results){
                    if(err){
                      console.log("Error making native query: "+JSON.stringify(err));
                      return res.json(
                        {
                          success: false,
                          message: "Error making native query"
                        }
                      );
                    } else{ 
                      // Adding the entities being followed to a single list
                      entitiesFollowed = results[0];
                      entitiesFollowed.push.apply(entitiesFollowed, results[1]);
                      entitiesFollowed.push.apply(entitiesFollowed, results[2]);

                      // Sorting the entitiesFollowed list on basis of id
                      entitiesFollowed.sortById = function() {
                          this.sort(function(a,b) {
                            if (b._id > a._id)
                              return 1;
                            else return -1;
                          });
                      };
                      entitiesFollowed.sortById();

                      return res.json(
                        {
                          success: true,
                          message: "Success Fetching Following",
                          following: entitiesFollowed
                        }
                      );

                    }
                  });
                }
              }
            );
          }
        }
      );
    },

    // Get User Details
    getDetail : function (req, res) {
      User.findOne({id : req.param('id')}, function(err, user){
        if(user){
          return res.json(user);
        }

        return res.json({
          error: 'not found',
          status: '404'
        });
        
      })
    },

    // Put User Details
    putDetail : function (req, res) {
      User.update({id : req.param('id')}, req.body , function(err, user){
        if(user){
          console.dir(user);
          return res.json(user);
        }
        console.log(err);
        console.dir(req.param('id'));
        return res.json({
          error: 'not found',
          status: '404'
        });
        
      })
    },

    // Get User List
    getList : function (req, res) {
      // Local use only !! Remove in production
      User.find(function(err, user){
        if(user){
          return res.json(user);
        }

        return res.json({
          error: 'not found',
          status: '404'
        });
        
      })
    },

    postList : function (req, res) {

        User.findOne({ email : req.body.email }, function(err, user) {
          if (user){
            return res.json({error: 'User already exists'},500);
          }

          else if((!err) && (!user)){
            User.create(req.body, function(err,user, info){
              if(user){
                return res.json({ message : 'user created',
                                  status : "201"}, 201);
              }
              else{   
                console.log(err);
                console.dir(info)

              }
            })
          }

          else {
            return res.json({
              error: err,
            }, 400)
          }

        })
    }
};
