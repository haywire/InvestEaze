var ObjectId = require('mongodb').ObjectID;

module.exports = {

  create: function(req, res){

    var request = req.body;
    console.log('Creating FundHouse: '+JSON.stringify(request));

    FundHouse.create(request).done(
      function(err, persistedFundHouse) {

          // Error handling
          if (err) {
            // The FundHouse could not be created!
            console.log("Error Persisting FundHouse: " + JSON.stringify(err));
            return res.json(
              {
                success: false,
                message: "Error Persisting FundHouse"
              }
            );
          } else {
            //FundHouse was created successfully!
            
            console.log("Success Persisting FundHouse: " + JSON.stringify(persistedFundHouse));
            return res.json(
              persistedFundHouse,
              201
            );
          }
      }
    );
  },

  find : function (req, res) { 
      console.log("Fetching FundHouse Details: " + JSON.stringify(req.body));

      var id = req.param('id');

       FundHouse.native(
        function(err, collection){
          collection.findOne(
            {
              _id: new ObjectId(id)/*,
              "isDeleted": false*/
            },
            {
              followers: 0,
              backers: 0,
              referrals: 0
            },
            function(err, fundHouse){
              if(err){
                  // FundHouse could not be found!
                  console.log("Error finding FundHouse: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error finding FundHouse"
                      },
                      404
                    );
              } else {
                return res.json(
                  fundHouse,
                  200
                );
              }
            }
          )
        }
      );
    },

    list: function(req, res) {
      
      console.log("Fetching FundHouses " + JSON.stringify(req.body));

      /*
      * Paginating the fundhouses list using page number and 
      * number of documents per page. Combined aprroach - range by "_id" + skip()
      * Using skip+limit is not a good way to do paging when performance is an issue,
      * or with large collections; it will get slower and slower as you increase the page number.
      * Using skip requires the server to walk though all the documents (or index values) from 0 
      * to the offset (skip) value. It is much better to use a range query (+ limit) where you pass
      * in the last page's range value.
      */

      var pageSize = req.param("size");
      var currentPageNumber = req.param("pageFrom");
      var newPageNumber = req.param("pageTo");
      var currentId = req.param("currentId"); // id of the first record on the current page
      var pageDiff = currentPageNumber - newPageNumber;
      var skip;

      // Loading Deafults
      if(!currentId)
        currentId = 0;
      if(!pageSize)
        pageSize = 10;
      if(!pageDiff)
        skip = 0;

      // Calculating the number of documents to skip and the queryParameter
      var queryParameter = {};
      if(pageDiff < 0) {

        if(!skip)
          skip = pageSize * ( ( (-1) * pageDiff) - 1);

        queryParameter = {
                          _id : 
                            {
                              $lt: new ObjectId(currentId)/*,
                              "isDeleted": false*/
                            }
                        }
      
      } else {
      
        if(!skip)
          skip = pageSize * pageDiff;
        
        queryParameter = {
                          _id : 
                            {
                              $gt: new ObjectId(currentId)/*,
                              "isDeleted": false*/
                            }
                        }
      }

      // Fetching FundHouses List
      FundHouse.native(
        function(err,collection){
          if(err){
            console.log("Error making native query: " + JSON.stringify(err));
            return res.json(
              {
                success: false,
                message: "Error making native query"
              }
            );
          } else {
              collection.find(
                queryParameter,
                {
                  name: 1,
                  aum: 1,
                  email: 1,
                  logoImage: 1
                }
              )
              .skip(skip)
              .limit(parseInt(pageSize))
              .sort({_id: 1}) // Sort by Id
              .toArray(
                function(err, fundHouses){
                  if(err){
                    // FundHouses could not be fetched!
                    console.log("Error Fetching FundHouses: "+ JSON.stringify(err));
                    return res.json(
                      {
                          success: false,
                         message: "Error Fetching FundHouses"
                      },
                      404
                    );
                  } else {
                    return res.json(
                      fundHouses,
                      200
                    );
                  }
                }
              );
            }
          }
        );
    },
    update : function (req, res) { 
      console.log("Updating FundHouse Details: " + JSON.stringify(req));

      var id = req.params('id');
      var updateParameters = req.body;

       FundHouse.native(
        function(err, collection){
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $set: updateParameters
            },
            function(err, fundHouse){
              if(err){
                  // FundHouse could not be updated!
                  console.log("Error updating FundHouse: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error updating FundHouse"
                      }
                    );
              } else {
                  return res.json(
                    {
                      success: true,
                      message: "Success Updating FundHouse",
                    }
                  );
              }
            }
          )
        }
      );
    },

    destroy : function (req, res) { 
      
      console.log("Soft Deleting FundHouse : " + JSON.stringify(req));

      var id = req.params('id');

      // Soft deleting FundHouse
      FundHouse.native(
        function(err, collection){
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $set: {"isDeleted": true}
            },
            function(err, fundHouse){
              if(err){
                  // FundHouse could not be soft deleted!
                  console.log("Error soft deleting FundHouse: "+ JSON.stringify(err));
                  return res.json(
                      {
                        success: false,
                        message: "Error soft deleting FundHouse"
                      }
                    );
              } else {
                return res.json(
                  {
                    success: true,
                    message: "Success soft deleting FundHouse",
                  }
                );
              }
            }
          )
        }
      );
    },

  // Add referrals to the FundHouse
	updateReferrals : function(req,res) {
		console.log('Content for updating referrals : '+ JSON.stringify(req));
		var id = req.id;

		FundHouse.native(
			function(err,collection){
	  		
  			collection.update(
  				{
  					_id: new ObjectId(id)
  				},
  				{
  					$addToSet: { 
  						refer: {
  							"initiator":req.initiator,
  							"initiated":req.initiated
  						} 
  					}
  				},
  				function(err,updatedFundHouse){
  					if(err){
  						// The FundHouse could not be updated!
	    				console.log("Error Updating FundHouse: "+err);
	  					return res.json(
	  							{
	  								success: false,
	  								message: "Error Updating FundHouse"
	  							}
	  						);
  					}
  					else {
  						// FundHouse was successfully updated!
	    				console.log("Success Updating FundHouse: " + updatedFundHouse);
	  					return res.json(
  							{
  								success: true,
  								message: "Success Updating FundHouse"
  							}
  						);
  					}
				  }
			  );
  		}
  	);
	},

  // Add followers to the FundHouse
	addFollower : function (req, res) {
      
    console.log("Adding follower to FundHouse: " + JSON.stringify(req));

    var followerId = req.followerId;
    var id = req.id;

    // Adding followee's Id to the Follower's following.. This is soo confusing!

    FundHouse.native(
      function(err,collection){
        
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $addToSet: { 
                followers: followerId
              }
            },
            function(err,updatedFundHouse){
            if(err){
              // FundHouse could not be updated!
              console.log("Error Adding Follower to FundHouse: "+ JSON.stringify(err));
              return res.json(
                  {
                    success: false,
                    message: "Error Adding Follower"
                  }
                );
            }
            else {
              // FundHouse was successfully updated!
              console.log("Success Adding Follower to FundHouse: " + updatedFundHouse);
              return res.json(
                  {
                    success: true,
                    message: "Success Adding Follower"
                  }
                );
            }
          }
          );
        }
      );

      // Adding follower's Id to the Followee's follower.. This is soo confusing!
      User.native(
        function(err,collection){
          
          collection.update(
            {
              _id: new ObjectId(followerId)
            },
            {
              $addToSet: { 
                following: {
                  id: id,
                  collection: "FundHouse"
                }
              }
            },
            function(err,updatedUser){
            if(err){
              // User could not be updated!
              console.log("Error Adding Following to User: "+ JSON.stringify(err));
              return res.json(
                {
                  success: false,
                  message: "Error Adding Follower"
                }
              );
            }
            else {
              // User was successfully updated!
              console.log("Success Adding Following to User: " + updatedUser);
              return res.json(
                {
                  success: true,
                  message: "Success Adding Follower"
                }
              );
            }
          }
        );
      }
    );
  },
  removeFollower : function (req, res) {
    
    console.log("Removing follower: " + JSON.stringify(req));

    var followerId = req.followerId;
    var id = req.id;

    // Removing followee's Id to the Follower's following.. This is soo confusing!

    FundHouse.native(
      function(err,collection){
        
          collection.update(
            {
              _id: new ObjectId(id)
            },
            {
              $pull: { 
                followers: followerId
              }
            },
            function(err,updatedFundHouse){
            if(err){
              // FundHouse could not be updated!
              console.log("Error Removing Follower from FundHouse: "+ JSON.stringify(err));
              return res.json(
                  {
                    success: false,
                    message: "Error Removing Follower"
                  }
                );
            }
            else {
              // FundHouse was successfully updated!
              console.log("Success Removing Follower from FundHouse: " + updatedFundHouse);
              return res.json(
                  {
                    success: true,
                    message: "Success Removing Follower"
                  }
                );
            }
          }
          );
        }
      );

      // Removing follower's Id to the Followee's follower.. This is soo confusing!
      User.native(
      function(err,collection){
        
          collection.update(
            {
              _id: new ObjectId(followerId)
            },
            {
              $pull: { 
                following: {
                  id: followeeId,
                  collection: "User"
                }
              }
            },
            function(err,updatedUser){
            if(err){
              // User could not be updated!
              console.log("Error Removing Following from User: "+ JSON.stringify(err));
              return res.json(
                  {
                    success: false,
                    message: "Error Removing Following"
                  }
                );
            }
            else {
              // User was successfully updated!
              console.log("Success Removing Following from User: " + updatedUser);
              return res.json(
                  {
                    success: true,
                    message: "Success Removing Following"
                  }
                );
            }
          }
        );
      }
    );
  },
  updateLikesCount: function(req, res) {

		var id = req.id;

    // Integer value by which numberOfLikes is to be updated
    // This could be either a positive or a negative value
    // Generally it is supposed to be 1 or -1 
		var incrementBy = req.counter;

		FundHouse.native(
      function(err,collection){
  			collection.update(
  				{
  					_id: new ObjectId(id)
  				},
  				{
					  $inc: { numberOfLikes: incrementBy }
				  },
  				function(err,updatedFundHouse){
  					if(err){
  						// Fund could not be Updated!
	    				console.log("Error Updating FundHouse: " + JSON.stringify(err));
	  					return res.json(
                {
							    success: false,
							    message: "Error Updating FundHouse"
						    }
              );
  					}
  					else {
  						// Fund was successfully Updated!
	    				console.log("Success Updating FundHouse: " + updatedFundHouse);
	  					return res.json(
                {
								  success: true,
							   	message: "Success Updating FundHouse"
							 }
              );
  					}
  				}
  			);
  	 }
    );
	},
	addBacker: function(req, res) {

		console.log("Adding backer to FundHouse: " + JSON.stringify(req));

		var id = req.id;
		var backerId = req.backerId;
		var amountInvested = req.amount;

		FundHouse.native(
      function(err,collection){
  			collection.update(
  				{
  					_id: new ObjectId(id)
  				},
  				{
  					$push: { 
  						backers: {
  							id : backerId,
  							amount : amountInvested,
  							date : new Date()
  						} 
  					}
  				},
    			function(err,updatedFundHouse){
    				if(err){
    					// FundHouse could not be Updated!
      				console.log("Error adding backer to FundHouse: " + JSON.stringify(err));
    					return res.json(
                {
  						    success: false,
  						  message: "Error adding backer to FundHouse"
  					    }
              );
    				}
    				else {
  						// FundHouse was successfully Updated!
      				console.log("Success adding backer to FundHouse: " + updatedFundHouse);

    					return res.json(
                {
  							  success: true,
  							  message: "Success adding backer to FundHouse"
  						  }
              );
  					}
    			}
  			);
  		}
    );
	},
  fetchFollowers : function (req, res) {

    var request = req.body;
    var id = req.param("id");
    
    // Paginating the followers list using page number and 
    // number of documents per page
    var pageSize = req.param("size");
    var pageNumber = req.param("page");

    // Calculating the number of documents to skip
    var skip = pageSize * (pageNumber - 1);

    FundHouse.native(
      function(err,collection){
        collection.findOne(
          {
            _id: new ObjectId(id)
          },
          {
            follower: {
              $slice:[
                skip, 
                parseInt(pageSize)
              ]
            },
            _id:0,
            team:0,
            tags:0,
            qna:0,
            pitch:0,
            backers:0,
            refer:0,
            terms:0,
            reviews:0
          },
          function(err, fundHouse){

            if(err){
              // FundHouse could not be found!
              console.log("Error finding FundHouse: "+ JSON.stringify(err));
              return res.json(
                {
                  success: false,
                  message: "Error finding FundHouse"
                }
              );
            } else{

              // FundHouse found!
              console.log("Success finding FundHouse: " + JSON.stringify(fundHouse));
             
              // Creating ObjectIds of followers from string ids,
              // to query from the database and storing in an array
              var followerIds = [];
              for(var i=0; i < fundHouse.follower.length; i++){
                followerIds.push(new ObjectId(fundHouse.follower[i]));
              }

              User.native(
                function(err,collection){
                  
                  collection.find(
                    {
                      _id: { "$in" : followerIds }
                    },
                    {
                      name: 1,
                      imageLink:1
                    }
                  )
                  .toArray(
                    function(err, followers){
                    
                      if(err){
                        // Followers could not be fetched
                        console.log("Error Fetching Followers: "+ JSON.stringify(err));
                        return res.json(
                          {
                            success: false,
                            message: "Error Fetching Followers"
                          }
                        );
                      } else {
                          return res.json(
                          {
                            success: true,
                            message: "Success Fetching Followers",
                            followers: followers
                          }
                        );
                      }
                    }
                  );
                }
              );
            }
          }
        )
      }
    );
  },
  fetchBackers : function (req, res) {

    var request = req.body;
    var id = req.param("id");

    // Paginating the backers list using page number and 
    // number of documents per page
    var pageSize = req.param("size");
    var pageNumber = req.param("page");

    // Calculating the number of documents to skip
    var skip = pageSize * (pageNumber - 1);

    FundHouse.native(
      function(err,collection){
        collection.findOne(
          {
            _id: new ObjectId(id)
          },
          {
            backers: {
              $slice:[
                skip,
                parseInt(pageSize)
              ]
            },
            _id:0,
            team:0,
            tags:0,
            qna:0,
            pitch:0,
            followers:0,
            refer:0,
            terms:0,
            reviews:0
          },
          function(err, fundHouse){

            if(err){
              // FundHouse could not be found!
              console.log("Error finding FundHouse: "+ JSON.stringify(err));
              return res.json(
                {
                  success: false,
                  message: "Error finding FundHouse"
                }
              );
            } else{

              // FundHouse found!
              console.log("Success finding FundHouse: " + JSON.stringify(fundHouse));
             
              // Generating ObjectIds from string ids of backers 
              // for querying database and storing them in an array
              var backerIds = [];
              for(var i=0; i < fundHouse.follower.length; i++){
                backerIds.push(new ObjectId(fundHouse.follower[i]));
              }

              User.native(
                function(err,collection){
                  
                  collection.find(
                    {
                      _id: { "$in" : backerIds }
                    },
                    {
                      name: 1,
                      imageLink:1
                    }
                  )
                  .toArray(
                    function(err, backers){
                      if(err){
                        // Backers could not be fetched
                        console.log("Error Fetching Backers: "+ JSON.stringify(err));
                        return res.json(
                            {
                              success: false,
                              message: "Error Fetching Backers"
                            }
                          );
                      } else {
                          return res.json(
                          {
                            success: true,
                            message: "Success Fetching Backers",
                            backers: backers
                          }
                        );
                      }
                    }
                  );
                }
              );
            }
          }
        )
      }
    );
  }
};