/**
 * AuthController
 */

 // Imports
var passport = require("passport"),
    configAuth = require('../../config/auth'),
    request = require('request'),
    Pusher = require('pusher');

// Initializing
var pusher = new Pusher({
  appId: '77682',
  key: '1d21f8b62586ce0ac86f',
  secret: '83214175fc4c5379c17f'
});  

module.exports = {
  // Basic Auth login 
  login: function(req,res){
    passport.authenticate( 'local',
      function(err, user, info){
        if ((err) || (!user)) {
          res.json({"login" : "false", "errors": info});
          console.log(err);
          return;
        }
        req.logIn( user,
          function(err){
            if (err){
              res.json(
                {
                  "login" : "false",
                  "errors": info
                }
              );
            }
            return res.json(
              {
                "login" : "true"
              }
            );
          }
        );
      }
    )(req, res);
  },

  /*
  // Local API key authentication 
  process: function(req,res){
    passport.authenticate('localapikey', function(err, user, info){
      if ((err) || (!user)) {
        res.redirect('/login');
        console.log(err);
        return;
      }
      req.logIn(user, function(err){
        if (err) res.redirect('/login');
        return res.redirect('/');

      });
    })(req, res);
  }, */


  logout: function (req,res){
    req.logout();
    res.json("logout : true");
  },


// =================== Facebook OAuth  ======================
  
  fbLogin: function (req, res) {
    passport.authenticate('facebook',
      { 
        // Mention the extended permissions needed by the app
        scope: ['email']
      },
      function(err, user, info){
        // if User chooses not to give permission to the app
        if ((err) || (!user)) {
          // 'redirect' view closes the popup window
          return res.view('redirect');
        }
        req.logIn(user,
          function(err){
            // login failed
            if (err){
              res.json(
                {
                  "login" : "false",
                  "errors": info
                }
              );
            }

            // login successful
            // closing the popup window via 'redirect' view 
            return res.view('redirect');
          }
        );
      }
    )(req, res);
  },

  fbCallback : function (req, res) {
    passport.authenticate('facebook',
      function(err, user, info){
        // if User chooses not to give permission to the app
        if((err) || (!user)){
          // 'redirect' view closes the popup window
          return res.view('redirect');
        } 
        req.logIn(user,
          function(err){
            // login failed
            if (err){
              res.json(
                {
                  "login" : "false",
                  "errors": err
                }
              );
            }

            // login successful
            // closing the popup window via 'redirect' view  
            return res.view('redirect');
          }
        );
      }
    )(req, res);
  },

  // facebook oAuth using accesToken
  fbToken : function (req, res) {
    passport.authenticate('facebook-token',
      function (err, user, info) {
        if((err) || (!user)){
          return res.send('/Error occured');
        } 

        req.logIn(user,
          function(err){
            // login failed
            if (err){
              res.json(
                {
                  "login" : "false",
                  "errors": err
                }
              );
            }

            // login successful 
            return res.send('/ Logged in');
          }
        );
      }
    )(req, res);
  },
// =================== <<< END >>> ======================


// =================== LinkedIn Auth  ======================

linkedinLogin: function (req, res) {
    passport.authenticate('linkedin',
      { 
        // Mention the permissions needed by the app 
        // LinkedIn gives basic profile by default
        scope: ['r_emailaddress']
      },
      function(err, user, info){
        // if User chooses not to give permission to the app
        if ((err) || (!user)) {
          // 'redirect' view closes the popup window
          return res.view('redirect');

        }
        req.logIn(user,
          function(err){
            // login failed
            if (err){
              res.json(
                {
                  "login" : "false",
                  "errors": err
                }
              );
            } 

            // login successful
            // closing the popup window via 'redirect' view  
            return res.view('redirect');
          }
        );
      }
    )(req, res);
  },

  linkedinCallback : function (req, res) {
    passport.authenticate('linkedin',
      function(err, user, info){
        // if User chooses not to give permission to the app
        if((err) || (!user)){
          // 'redirect' view closes the popup window
          return res.view('redirect');
        } 

        req.logIn(user,
          function(err){
            // login failed
            if (err){
              console.log(err);
              res.json(
                {
                  "login" : "false",
                  "errors": err
                }
              );
            } 

            // login successful
            // closing the popup window via 'redirect' view  
            return res.view('redirect');
          }
        );
      }
    )(req, res);
  },

// =================== <<< END >>> ======================

// =================== Twitter Auth  ======================

twitterLogin: function (req, res) {
    passport.authenticate('twitter',
      function(err, user, info){
        // if User chooses not to give permission to the app
        if ((err) || (!user)) {
          // 'redirect' view closes the popup window
          return res.view('redirect');
        }
        req.logIn(user,
          function(err){
            // login failed
            if (err){
              res.json(
                {
                  "login" : "false",
                  "errors": err
                }
              );
            }

            // login successful
            // closing the popup window via 'redirect' view
            return res.view('redirect');
          }
        );
      }
    )(req, res);
  },

  twitterCallback : function (req, res) {
     passport.authenticate('twitter',
        function(err, user, info){
          // if User chooses not to give permission to the app
            if((err) || (!user)){
              // 'redirect' view closes the popup window
              return res.view('redirect');
            } 
            req.logIn(user, function(err){
              if (err){
                // login failed
                res.json(
                  {
                    "login" : "false",
                    "errors": err
                  }
                );
              }
              // login successful
              // closing the popup window via 'redirect' view 
              return res.view('redirect');
          }
        );
      }
    )(req, res);
  },

  twitterToken : function (req, res) {
    passport.authenticate('twitter-token',
      function(err, user, info){
        if((err) || (!user)){
          return res.json(
            {
              errors : err
            }
          );
        } 

        req.logIn(user,
          function(err){
            // login failed
            if (err){
              res.json(
                {
                  "login" : "false",
                  "errors": err
                }
              );
            } 

            // login successful
            return res.json(
              {
                message : 'logged in'
              }
            );
          }
        );
      }
    )(req, res);
  },

  /*
  twitterRev : function (req, res) {
  
    var self = this;
    request.post({
      url : 'https://api.twitter.com/oauth/request_token',
      oauth : { 
        consumerKey: configAuth.twitterAuth.consumerKey,
        consumerSecret: configAuth.twitterAuth.consumerSecret
      },
      form : { x_auth_mode : 'reverse_auth'}
    }, function(err, r, body){
      if(err){
        return res.send(500, { message : e.message });
      }

      if(body.indexOf('OAuth') !== 0){
        return res.send(500, {message : 'Malformed response from twitter'});
      }

      res.send({ x_reverse_auth_parameters : body });
    });
  },
*/
// =================== <<< END >>> ======================

// =================== Auth user check  ======================
  
  // gives currently authenticated user
  getAuthUser : function(req, res){
    if(req.isAuthenticated()){
      res.json(
        {
          login : true,
          userId : req.session.passport.user 
        }
      ) 
    } else{
      res.json(
        {
          login : false,
          userId : null 
        }
      )
    }
  },

  pusherAuth: function (req, res){

    console.log("Authenticating pusher channel: " + JSON.stringify(req.body));
    var channel_name = req.param('channel_name');
    var socket_id = req.param('socket_id');
    var userId = '5386daf7ae6bbae00d54ae4a'; //req.session.passport.user
    
    var presenceData = { user_id: userId };
    var auth = pusher.auth( socket_id, channel_name, presenceData );
    return res.json(
                auth,
                200
              );
  },

// =================== <<< END >>> ======================

  noshow: function (req,res){
    res.json(
      {
        message : 'nothing to show'
      } 
    );
  },

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to AuthController)
   */
   
  _config: {}

  
};
