// Imports
var Pusher = require('pusher');

// ObjectID helps in creating mongodb type id for querying database
var ObjectId = require('mongodb').ObjectID;

var pusher = new Pusher({
  appId: '77682',
  key: '1d21f8b62586ce0ac86f',
  secret: '83214175fc4c5379c17f'
});

module.exports = {

    // Fetch notification
    find : function(req, res){

        var id = req.param('id');

        Notification.native(
            function(err, collection){
              collection.findOne(
                {
                  _id: new ObjectId(id)
                },
                function(err, notification){
                  if(err){
                      // Notification could not be found!
                      console.log("Error finding Notification: "+ JSON.stringify(err));
                      return res.json(
                          {
                            success: false,
                            message: "Error finding Notification"
                          }
                        );
                  } else {
                    return res.json(
                      {
                        success: true,
                        message: "Success finding Notification",
                        notification: notification
                      }
                    );
                  }
                }
              )
            }
        );
    },

    list: function(req, res) {

      var request = req.body;
      var userId = req.param('userId');

      console.log("Fetching Notifications " + userId);

      /*
      * Paginating the users list using page number and 
      * number of documents per page. Combined aprroach - range by "_id" + skip()
      * Using skip+limit is not a good way to do paging when performance is an issue,
      * or with large collections; it will get slower and slower as you increase the page number.
      * Using skip requires the server to walk though all the documents (or index values) from 0 
      * to the offset (skip) value. It is much better to use a range query (+ limit) where you pass
      * in the last page's range value.
      */

      var pageSize = req.param("size");
      var currentPageNumber = req.param("pageFrom");
      var newPageNumber = req.param("pageTo");
      var currentId = req.param("currentId"); // id of the first record on the current page
      var pageDiff = currentPageNumber - newPageNumber;
      var skip;

      var queryJson = {};

      if(!pageSize)
        pageSize = 10;
      if(!pageDiff){
        skip = 0;
      }

      // Loading Deafults
      if(!currentId)
      {
        if(pageDiff < 0) {

          skip = pageSize * ( ( (-1) * pageDiff) - 1);
          queryJson = {
                        userId : userId
                      }
        } else {
        
          skip = pageSize * pageDiff;
          queryJson = {
                        userId : userId
                      }
        }

      } else{
        
        currentId = new ObjectId(currentId); 

        if(pageDiff < 0) {

          skip = pageSize * ( ( (-1) * pageDiff) - 1);
          queryJson = {
                            _id : 
                              {
                                $lt: currentId
                              },
                              userId : userId

                          }
        
        } else {
        
          skip = pageSize * pageDiff;
          queryJson = {
                            _id : 
                              {
                                $gt: currentId
                              },
                              userId : userId
                          }
        }
      
      }

      // Fetching Users List
      Notification.native(
        function(err,collection){
          if(err){
            console.log("Error making native query: " + JSON.stringify(err));
            return res.json(
              {
                success: false,
                message: "Error making native query"
              }
            );
          } else {
              collection.find(
                queryJson
              )
              .skip(skip)
              .limit(parseInt(pageSize))
              .sort({_id: 1}) // Sort by Id
              .toArray(
                function(err, notifications){
                  if(err){
                    // Users could not be fetched!
                    console.log("Error Fetching Notifications: "+ JSON.stringify(err));
                    return res.json(
                      {
                          success: false,
                         message: "Error Fetching Notifications"
                      }
                    );
                  } else if(!notifications.length){
                      console.log('No Notifications for this User!');
                      return res.json([], 200);
                  } else {

                    var notificationsDetails = [];

                    sails.controllers.user.findName(
                      userId,
                      function(userDetails){

                        if(userDetails){
                          
                          console.log("User: " + JSON.sringify(userDetails));

                          var counter = 0;

                          for(var i=0; i<notifications.length; i++){

                            if(notifications[i].collection = "Activity"){

                              console.log("Activity");

                              sails.controllers.activity.getDetails(
                                notifications[i].collectionId,
                                function(activity){

                                  activity.userName = userDetails.name;
                                  activity.image = userDetails.imageLink;
                                  notificationsDetails.push(activity);

                                  counter++;

                                  if(counter == notifications.length){
                                    return res.json(
                                      notificationsDetails,
                                      200
                                    );
                                  }
                                }
                              )

                            } else if(notifications[i].collection = "Post"){

                              sails.controllers.post.getDetails(
                                notifications[i].collectionId,
                                function(post){
                                  post.userName = userDetails.name;
                                  post.image = userDetails.imageLink;
                                  notificationsDetails.push(post);

                                  counter++;

                                  if(counter == notifications.length){
                                    return res.json(
                                      notificationsDetails,
                                      200
                                    );
                                  }

                                }
                              );

                            } else if(notifications[i].collection = "Comment") {
                              
                              sails.controllers.comment.getDetails(
                                notifications[i].collectionId,
                                function(comment){
                                  comment.userName = userDetails.name;
                                  comment.image = userDetails.imageLink;
                                  notificationsDetails.push(comment);

                                  counter++;

                                  if(counter == notifications.length){
                                    return res.json(
                                      notificationsDetails,
                                      200
                                    );
                                  }
                                }
                              );
                            }
                          }
                        }
                        else{
                          console.log("User Not Found!");
                        }
                      }
                    );
                  }
                }
              );
            }
          }
        );
    },

    // Create a Notification
    create : function (req, res) {

        var userId = req.body.userId;

        console.log("\nCreating Notification for User: "+userId);
        
        Notification.create(req.body, 
            function(err, notification, info){ //need to correct the parameter
                if(notification){

                    console.log("Notification created");

                   /* if(sails.config.globals.onlineNotifications[req.body.userId]) {
                        console.log("User " + req.body.userId + " is currently logged in. Sending Notification message..");
                        sails.io.sockets.socket(sails.config.globals.onlineNotifications[req.body.userId]).emit('notificationReceived', req.body);
                    }*/

                    //=== Pusher code

                    /*
                    * Sending notification to the user if the user is online, i.e.
                    * its notification channel is active. We assume that the notification 
                    * channel for each user is of the form:
                    * presence-notificationChannel-userId
                    */

                    var channelName = 'presence-notificationChannel-' + userId;

                    /*
                    * Check for the member count of the user's notification channel to 
                    * check whether the user is online or not.
                    */

                    pusher.get( { path: '/channels/'+channelName+'/users', params: {} },
                        function( error, request, response ) {
                          if( response && response.statusCode === 200 ) {
                              
                              if(JSON.parse(response.body).users.length > 0){


                                if(req.body.collection == "Post"){

                                  sails.controllers.post.getDetails(
                                    req.body.collectionId,
                                    function(post){
                                      pusher.trigger(
                                        channelName,
                                        'newNotification', 
                                        post
                                      );
                                    }
                                  );

                                } else if(req.body.collection == "Comment") {

                                  sails.controllers.comment.getDetails(
                                    req.body.collectionId,
                                    function(comment){
                                      pusher.trigger(
                                        channelName,
                                        'newNotification', 
                                        comment
                                      );
                                    }
                                  );

                                } else if (req.body.collection == "Activity") {

                                  sails.controllers.activity.getDetails(
                                    req.body.collectionId,
                                    function(activity){

                                      console.log("\nSending Notification to channel: "+channelName);

                                      pusher.trigger(
                                        channelName,
                                        'newNotification', 
                                        activity
                                      );
                                    
                                    }
                                  );
                                }
                            }
                          }
                        }
                    );

                    return res.json(
                        { 
                            message : 'Notification created',
                            success : true
                        }, 
                        201
                    );
                }

                return res.json(
                    { 
                        error: "no",
                        success : false
                    },
                    400
                );        
        })
    },

    // Remove a Notification
    destroy : function(req, res) {

        var queryJson = {};
        
        if(req.param){
            var id = req.param('id');
            queryJson = {
                        _id: new ObjectID(id)
                    };
        } else {
            queryJson = req.queryJson;
        }

        Notification.native(
            function(err,collection){
                collection.remove(
                    queryJson,
                    1,
                    function(err,removedNotification){
                        if(err){
                            // The Notification could not be Removed!
                            console.log("Error Removing Notification: " + JSON.stringify(err));
                            return res.json(
                                {
                                    success: false,
                                    message: "Error Removing Notification"
                                }
                            );
                        }
                        else {
                            // Notification was successfully Removed!
                            console.log("Success Removing Notification: " + removedNotification);
                            return res.json(
                                {
                                    success: true,
                                    message: "Success Removing Notification"
                                }
                            );
                        }
                    }
                );
            }
        );
    },

    connect: function(req, res){

        // Fetching socket from the request
        var socket = req.socket;

        // UserId which is sent from the client side
        var userId = req.param('userId');

        console.log('Notification Socket Connected for user: ' + userId);

        // setting username for the socket
        socket.username = userId;

        /*
        * Check if the user is already active, i.e if the user already exists in the 
        * onlineNotifications list. onlineNotifications list keeps the active userId as the key and 
        * socketId as the value corresponding to it. This helps us to find out if the 
        * recepient of a message in currently online or not, so that we can emit message to
        * corresponding socket for the recepient.
        */
        if(sails.config.globals.onlineNotifications[userId])
                console.log("User already active");
        else {
            sails.config.globals.onlineNotifications[userId] = socket.id;
            console.log("User now active: " + userId);
        }

        socket.on('disconnect',
            function(){
                // Socket is disconnected, we need to delete the userId from global onlineNotifications list.
                delete sails.config.globals.onlineNotifications[socket.username];
                console.log("User disconnected: "+socket.username);
            }
        );

        return res.json(
            { 
                success : true
            },
            200
        );    
    }



}