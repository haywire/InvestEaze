
define(["backbone"], function(Backbone) {

	var User = Backbone.Model.extend({
		defaults: {
			accredited: false,
			badges: [],
			followers: [],
			following: [],
			name: '',
			dob: '',
			username: '',
			email: '',
			org_name: 'Twitter Inc', 
			street:'795 Folsom Ave',
			house: 'Suite 600',
			city: 'San Francisco',
			state: 'CA',
			pin: '94107',
			country: 'USA',
			phone: '(123) 456-7890',
			imageLink: '',
			pro_title: [
				{
					text: 'Investing Ninja',
					company: '',
				},
				{
					text: 'CEO',
					company: 'Infoassembly',
				}  
			],
			investments: 
        [
          {amount:123.32, fundId: "someId", date: "Linux Date"},
          {amount:223.32, fundId: "someOtherId", date: "Linux Date 2"}
        ], 

        	noOfPosts:'45',
        	noOfIdeas:'32',
			previousCompanies: [],
			profile_facebook : false,
			profile_twitter  : false,
			profile_linkedIn : false,
	
			tags: [
				{ text:'Developer' },
				{ text:'Finance' }
			],
		},
		url : function() {
			return this.id ? '/user/' + this.id : '/user';
		} 
	})

	return User;

})