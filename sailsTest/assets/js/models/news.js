define(["backbone"], function (Backbone){

  var NewsModel = Backbone.Model.extend({

    urlRoot: '/news',
    defaults:{
      link:'',
      articleType: '',
      date:'',
      entityCollection:0,
      entityId:0
    }
  });
  return NewsModel;
});