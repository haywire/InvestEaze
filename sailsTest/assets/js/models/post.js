define(["backbone"], function(Backbone) {

	var Post = Backbone.Model.extend({

		urlRoot: '/post',
		defaults: {
			initiator : '',
			postType : '',
			content : '',
			numberOfLikes : 0,
			numberOfComments : 0,
			numberOfShares : 0,
			sharedFrom : '',
			ticker:'',
			advice:'',
			trend:''
		}
	})

	return Post;

})