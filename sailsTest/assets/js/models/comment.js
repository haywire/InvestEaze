define(["backbone"], function(Backbone) {

	var Comment = Backbone.Model.extend({
		default: {
			numberOfLikes: 0
		},
        url: function () {
            return '/comment';
          }
	})

	return Comment;

})