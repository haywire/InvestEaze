
define(['react'], function(React) {

  var SlideItem = React.createClass({
    render: function() {
      var rows = this.props.rows.map(function (row) {
        return (
          <div key={row.id}>
            <div className="col-md-6" >
              <span>{row.column1}</span>
            </div>
            <div className="col-md-6" >
              <span>{row.column2}</span>
            </div>
          </div>
        );
      });
      return (
        <div className="col-md-4">
          <div className="text-center well well-sm">
            <span>{this.props.header}</span>
            <div className="row">
              {rows}
            </div>
          </div>
        </div>
      );
    }
  })

  return SlideItem;

})