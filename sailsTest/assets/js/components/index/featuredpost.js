
define(['react'], function(React) {

  var FeaturedPost = React.createClass({
    render: function() {
      return (
        <div className="row-fluid">
          <div className="col-md-10">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">{this.props.data.title}<small className="pull-right">{this.props.data.time}</small></h3>
                <small className="grey">{this.props.data.titleContent}</small>
              </div>
              <div className="text-center col-md-12 featuredImgDiv noPadding">
                <img src={this.props.data.content.banner} />
              </div>
              <div className="panel-body">
                <div className="media">
                  <a className="pull-left" href="#">
                    <img className="media-object" src={this.props.data.content.image} className="profileImage" />
                  </a>
                  <div className="media-body">
                    <h4 className="media-heading">{this.props.data.content.title}</h4>
                    {this.props.data.content.text}
                  </div>
                </div>
              </div>
              <div className="panel-footer">
                <a href="#"><span className="fa fa-arrow-circle-up marginr5p"> {this.props.data.upvotes}</span></a>
                <a href="#"><span className="fa fa-arrow-circle-down marginr5p"> {this.props.data.downvotes}</span></a>
                <a href="#"><span className="fa fa-comment marginr5p"> {this.props.data.comments}</span></a>
                <a href="#"><span className="fa fa-share marginr5p"> {this.props.data.shares}</span></a>
              </div>
            </div>
          </div>
        </div>
      );
    }
  })

  return FeaturedPost;

})