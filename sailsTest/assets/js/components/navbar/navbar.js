define(["react", "jsx!components/notification/notification"], function(React, Notification) {

	var NavigationBar=React.createClass({
		visitProfile: function(e) {
			this.props.sendIdToParent(e.target.getAttribute("data-id"));
		},
		render: function()  {
			return(
				<div className="navbar navbar-default navbar-static-top" role="navigation" style={{"height": "60px"}}>
				<div className="container-fluid">
				<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<PrimaryTabs updateTabState={this.props.updateTabState}/>
        <ul className='nav navbar-nav navbar-right'>
          <SearchBar />
          <Messages updateState={this.props.updateState}/>
          <Notification userId={this.props.userId} updateState={this.props.updateState}/>
          <Settings updateState={this.props.updateState} />
        </ul>
				</div>
				</div>
				</div>
				)
		}
	});

  var PrimaryTabs = React.createClass({
    handleClick: function(state){
      this.props.updateTabState(state);
      $('.cs-navbar-items').removeClass('active-tab');
      $('.'+ state.toLowerCase()).addClass('active-tab');
    },
    render: function(){
     return (
       <ul className="nav navbar-nav">
       <li className='cs-navbar-items active-tab portfolio' onClick={this.handleClick.bind(null, 'Portfolio')}><a href="javascript:void(0);">Portfolio</a></li>
       <li className='cs-navbar-items news' onClick={this.handleClick.bind(null, 'News')}><a href="javascript:void(0);">News</a></li>
       <li className='cs-navbar-items explore' onClick={this.handleClick.bind(null, 'Explore')}><a href="javascript:void(0);">Explore</a></li>
       <li className='cs-navbar-items'><Logo /></li>
       </ul>
       )
    }
  });

	var SearchBar=React.createClass({
		render: function() {
			return( 
				<li style={{'padding-top': '5px'}}>
				<form className="navbar-form" role="search">
				<div className="form-group">
				<input type="text" className="form-control" style={{"width" : "200px"}} placeholder="Search for people, news and investments"/>
				</div>
				</form>
				</li>
				)
		}
	})

	var Settings=React.createClass({
    handleClick : function(){
      this.props.updateState('Settings');
    },
		render:function(){
			return(
				<li className="dropdown cs-navbar-items" style={{'width': '115px !important'}}>
				<a className="dropdown-toggle" data-toggle="dropdown">Settings <b className="caret"></b></a>
				<ul className="dropdown-menu">
				<li><a href="#">Action</a></li>
				<li><a href="#">Another action</a></li>
				<li><a href="#">Something else here</a></li>
				<li className="divider"></li>
				<li><a href="#">Separated link</a></li>
				</ul>
				</li>)
		}
	})


	var Home=React.createClass ({
		render:function(){
			return(
				<li className='cs-navbar-items'><a href="#">Home</a></li>)
		}
	})


	var UserName=React.createClass ({
		render:function(){
			return(
				<li className='cs-navbar-items'><a href="#">UserName</a></li>)
		}
	})



	var Notifications=React.createClass ({
    handleClick : function(){
      this.props.updateState('Notifications');
    },
		render:function(){
			return(
				<li className='cs-navbar-items' onClick={this.handleClick}><a href="javascript:void(0);">Notifications</a></li>)
		}
	})




	var Messages=React.createClass ({
    handleClick : function(){
      this.props.updateState('Messages');
    },
		render:function(){
			return(
				<li className='cs-navbar-items' onClick={this.handleClick}><a href="javascript:void(0);">Messages</a></li>)
		}
	})


	var Logo=React.createClass ({
    handleClick : function(){
      window.location.href = '#/home';
    },
		render:function(){
			return(
				<li className='cs-navbar-items' style={{'margin-left': '150px'}} onClick={this.handleClick}><a href="#">InvestEaze</a></li>)
		}
	});

	return NavigationBar;

})
