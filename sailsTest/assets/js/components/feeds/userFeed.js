define(["react", 'moment', "jsx!components/misc/simplePost", "jsx!components/misc/post", "jsx!components/misc/feed", "jsx!components/profile/activity", "models/user", "models/post", "models/activity", "collections/post", 'models/message'], function (React, moment, SimplePostComponent, Post, Feed, UserActivity, User, PostModel, Activity, PostCollection, Message) {
  var UserFeeds = React.createClass({
    setActive: function (e) {
      this.setState({activeTab: e.target.innerHTML});
    },

    getDefaultProps: function () {
      return {
        activeTabPane: "tab-pane active"
      }
    },
    getInitialState: function () {
      return {
        postItems: [],
        headlines: [],
        postText: '',
        viewPost: "all",
        user: {},
        activeTab: 'Activity',
        followers: 0
      }
    },
    expandStatusBox: function (e) {
      e.target.setAttribute("rows", "3");
    },
    handlePostChange: function (e) {
      this.setState({postText: e.target.value});
    },
    followUser: function () {
      var follow = new Activity();
      var followDetails = {
        initiator: String(this.props.userId),
        parentId: String(this.props.otherUser),
        parentCollection: 'User',
        activityType: 'Follow'
      };
      follow.save(followDetails, {
        success: function (follow) {
        }
      })

      var followBtn = $('.followButton');
      followBtn.toggleClass('follow').text(followBtn.hasClass('follow') ? 'Unfollow' : 'Follow');
    },
    sendPost: function (e) {
      console.log("sendposts called")
      if(e.keyCode == 13){

        var postModel = new PostModel();
        var destinationId = null;
        if (this.props.otherUser && this.props.otherUser != this.props.userId) {
          destinationId = this.props.otherUser
        }

        postModel.save(
          {
            initiator: this.props.userId,
            postType: 'Post',
            content: this.state.postText,
            postDestinationOwner: destinationId
          },
          {
            url: '/post',
            success: function (model, response, options) {
              this.state.postItems.unshift(response);
              this.setState({postItmes: this.state.postItems, postText: ''});

            }.bind(this),
            error: function (resp) {
              /* POST COULD NOT BE UPDATED */
            }
          }
        )
      }
    },
    getProfiles: function (id, cb) {
      var user = new User();
      user.set({id: id});
      user.fetch({
        success: function (response) {
          cb(response);
        }
      })

    },
    getProfile: function (id) {
      var user = new User();
      user.set({id: id});
      this.setState({user: user});
      var that = this;
      user.fetch({
        success: function (response, user) {
          that.setState({user: response});
          that.forceUpdate();
        }
      })
      /* User data fetched. */
    },
    getPosts: function (id) {
      /* Fetch posts relating to this user */
      console.log('got called')
      var postModel = new PostCollection();
      this.setState({postItems: postModel});
      var postings = postModel.fetchCurrent(id, function (posts) {
          this.setState({postItems: posts});
        }.bind(this)
      );
      /* Posts fetched */
    },
    message: function () {
      var x;
      var message = prompt("Please enter your message");
      if (message != null) {
        console.log(message);
        var messageModel = new Message();
        messageModel.save(
          {
            to : this.props.otherUser,
            from : this.props.userId,
            content : message
          },
          {
            url: '/message',
            success: function(model, response, options) {
              alert('Your message has been sent successfully');
            }.bind(this),
            error: function(resp) {
              /* POST COULD NOT BE UPDATED */
            }
          }
        )
      }


    },

    componentWillReceiveProps: function (nextProps) {
      if(!this.props.otherUser){
        this.ownProfile = true;
        this.getProfile(this.props.userId);
        this.getPosts(this.props.userId);
      } else if(this.props.otherUser && this.props.userId == this.props.otherUser){
        console.log('Own Profile');
        this.ownProfile = true;
        this.getProfile(this.props.userId);
        this.getPosts(this.props.userId);
      } else {
        this.ownProfile = false;
        console.log('Other User Profile');
        this.getProfile(this.props.otherUser);
        this.getPosts(this.props.otherUser);
      }
    },

    componentWillMount: function () {
      if(!this.props.otherUser){
        this.ownProfile = true;
        this.getProfile(this.props.userId);
        this.getPosts(this.props.userId);
      } else if(this.props.otherUser && this.props.userId == this.props.otherUser){
        console.log('Own Profile');
        this.ownProfile = true;
        this.getProfile(this.props.userId);
        this.getPosts(this.props.userId);
      } else {
        this.ownProfile = false;
        console.log('Other User Profile');
        this.getProfile(this.props.otherUser);
        this.getPosts(this.props.otherUser);
      }
    },
    componentDidMount: function (){

    },
    render : function (){
      var user = this.state.user;
      var that = this;
      var noOfInvestments = user.get('investments').length || 0;
      var followers = user.get('followers').length || 0;
      var following = user.get('following').length || 0;
      var Posts = this.state.postItems.map(function (post) {
        return <SimplePostComponent postData={post} userData={user} key={post.get('id')} showUserProfile={that.props.showUserProfile}/>
      });
      var followBtn, messageBtn;
      if(this.ownProfile){
        messageBtn = <button className='btn btn-default btn-xs' style={{'float': 'right', 'margin-top': '5px'}}>Edit Profile </button> ;
      } else {
        messageBtn = <button className='btn btn-default btn-xs messageButton' onClick={this.message} style={{'float': 'right', 'margin-top': '5px'}}>Message </button>;
        if(user.get('followers').indexOf(this.props.otherUser) != -1){
          followBtn = <button className='btn btn-default btn-xs followButton' style={{'float': 'right', 'margin-top': '5px'}} onClick={this.followUser}>Follow </button>;
        } else {
          followBtn = <button className='btn btn-default btn-xs followButton follow' style={{'float': 'right', 'margin-top': '5px'}} onClick={this.followUser}>Unfollow </button>;
        }
      }
      return (
        <div className='social'>
          <div className={ this.state.activeTab == 'Activity' ? this.props.activeTabPane : "tab-pane" } >
              <div className="col-md-12 margint10 marginb20 user-feed-top">
                <div className="col-md-3" style={{'padding': '0px', 'width': '50px'}}>
                  <img src={ user.get('imageLink') } height='58' width='58' className="profileImage img-responsive" alt="Responsive image"/>
                </div>
                <div className="col-md-10">
                  <div style={{'font-weight': 'bold'}}> {user.get('name')}</div>
                  <div>
                    <div className='social-profile-header'> Followers <br/> {followers} </div>
                    {(function (){
                      if(user.get('type') == 'Fund'){
                        return '';
                      } else {
                        return <div className='social-profile-header'> Following <br/> {following} </div>;
                      }
                    })()}
                    <div className='social-profile-header'> Investments <br/> {noOfInvestments} </div>
                  </div>
                  <div>
                    {(function (){
                      if(user.get('type') == 'Fund'){
                        return '';
                      } else {
                        return messageBtn;
                      }
                    })()}
                  </div>
                  <div style={{'clear': 'both'}}> {followBtn} </div>
                </div>
                <textarea className="form-control" style={{'width': '372px', 'margin-top': '4px'}} onKeyUp={this.sendPost} placeholder="Share an Update" rows="1" value={this.state.postText} onClick={this.expandStatusBox} onChange={this.handlePostChange}></textarea>
              </div>
                     {Posts}
          </div>
          </div>
      );
    }
  });

  return UserFeeds;
});
