define(["react", 'moment', "jsx!components/misc/simplePost", "jsx!components/misc/post", "jsx!components/misc/feed", "jsx!components/profile/activity", "models/user", "models/post", "models/activity", "collections/post"], function (React, moment, SimplePostComponent, Post, Feed, UserActivity, User, PostModel, Activity, PostCollection) {
  var PublicFeeds = React.createClass({
    setActive: function (e) {
      this.setState({activeTab: e.target.innerHTML});
    },

    getDefaultProps: function () {
      return {
        activeTabPane: "tab-pane active"
      }
    },
    getInitialState: function () {
      return {
        postItems: [],
        headlines: [],
        postText: '',
        viewPost: "all",
        user: {},
        activeTab: 'Activity',
        followers: 0
      }
    },
    expandStatusBox: function (e) {
      e.target.setAttribute("rows", "3");
    },
    handlePostChange: function (e) {
      this.setState({postText: e.target.value});
    },
    sendPost: function (e) {
      console.log("sendposts called")
      if(e.keyCode == 13){

        var postModel = new PostModel();
        var destinationId = null;
        if (this.props.param_id && this.props.param_id != this.props.userId && document.URL == root + '/#/profile/' + String(this.props.param_id)) {
          destinationId = this.props.param_id
        }

        postModel.save(
          {
            initiator: this.props.userId,
            postType: 'Post',
            content: this.state.postText,
            postDestinationOwner: destinationId
          },
          {
            url: '/post',
            success: function (model, response, options) {
              this.state.postItems.unshift(response);
              this.setState({postItmes: this.state.postItems, postText: ''});

            }.bind(this),
            error: function (resp) {
              /* POST COULD NOT BE UPDATED */
            }
          }
        )
      }
    },
    getProfiles: function (id, cb) {
      var user = new User();
      user.set({id: id});
      user.fetch({
        success: function (response) {
          cb(response);
        }
      })

    },
    getProfile: function (id) {
      var user = new User();
      user.set({id: id});
      this.setState({user: user});
      var that = this;
      user.fetch({
        success: function (response, user) {
          that.setState({user: response});
          that.forceUpdate();
        }
      })
      /* User data fetched. */
    },
    getPosts: function (id) {
      /* Fetch posts relating to this user */
      console.log('got called')
      var postModel = new PostCollection();
      this.setState({postItems: postModel});
      var postings = postModel.fetchCurrent(id, function (posts) {
          this.setState({postItems: posts});
        }.bind(this)
      );
      /* Posts fetched */
    },
    message: function () {
      var x;
      var message = prompt("Please enter your message");
      if (message != null) {
        console.log(message);
      }
    },
    componentWillReceiveProps: function (nextProps) {
      //temporary solution, will be fixing this soon enough
      if (nextProps.param_id && document.URL == root + '/#/profile/' + String(nextProps.param_id)) {
        this.getProfile(nextProps.param_id);
        console.log('yay for props paramas')
        this.getPosts(nextProps.param_id);

      }

      else {
        console.log("User", nextProps.userId);
        this.getProfile(nextProps.userId);
        console.log('yay for props user')

        this.getPosts(nextProps.userId);
      }
    },

    componentWillMount: function () {
      this.currentUserId = this.props.userId;
      //temporary solution, will be fixing this soon enough
      if (this.props.param_id && document.URL == root + '/#/profile/' + String(this.props.param_id)) {
        this.getProfile(this.props.param_id);
        console.log('yay for comp paramas')

        this.getPosts(this.props.param_id);
      }
      else {
        this.getProfile(this.props.userId);
        console.log('yay for comp user')

        this.getPosts(this.props.userId);
      }
    },
    render : function (){
      var user = this.state.user;

      return (
        <div className='social'>
          <div className={ this.state.activeTab == 'Activity' ? this.props.activeTabPane : "tab-pane" } >
            <div className="col-md-12 margint10 marginb20 user-feed-top">
              <div className="col-md-3" style={{'padding': '0px', 'width': '50px'}}>
                <img src={ user.get('imageLink') } onClick={this.props.showUserProfile.bind(null, this.props.userId)} height='58' width='58' className="profileImage img-responsive" alt="Responsive image"/>
              </div>
              <div className="col-md-8">
                <div style={{'font-weight': 'bold'}}> {user.get('name')}</div>
                <textarea className="form-control" style={{'width': '308px', 'margin-top': '4px'}} onKeyUp={this.sendPost} placeholder="Share an Update" rows="1" value={this.state.postText} onClick={this.expandStatusBox} onChange={this.handlePostChange}></textarea>
              </div>
            </div>
                     <Feed id = {this.props.userId} userObj={this.state.user} showUserProfile={this.props.showUserProfile}/>
          </div>
        </div>
        );
    }
  });

  return PublicFeeds;
});
