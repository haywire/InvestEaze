
define(["react", 'jsx!components/news/newsItem', 'models/news'], function(React, NewsItem, NewsModel){
	var News = React.createClass({
		getInitialState: function(){
			return {
				newsItems: []
			}
		},
    getNews: function(){
      var that = this;
      console.log('Getting News for the Day');
      var newsModel = new NewsModel();
      console.log(newsModel);
      newsModel.fetch({
        success: function(bbModel, newsItems){
          that.setState({
            newsItems: newsItems
          })
        }
      })
    },
    componentWillMount: function(){
      this.setState({
        newsItems: [
          {
            'headline': 'This is news one headline',
            'description': 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
            'newsImageURL': '/images/rupeesfiscal.jpg',
            'content': '<div></div><b><a class="place" href="http://www.ndtv.com/article/list/cities/1/bhopal" >Bhopal</a>:&nbsp;</b>The Special Task Force of the Madhya Pradesh Police has arrested 50 people -- including, students, parents and alleged brokers -- in connection with the multi-crore' +
              'Madhya Pradesh Professional Examination Board scam.<br /><br />Earlier in the day, the STF had conducted raids across the state and detained over 100' +
              'people over the education scam which initially surfaced in 2012-2013.<br /><br />The state authorities were forced to cancel the admission of over 1,000 students after it was revealed that they had gained admission to government medical colleges by fraudulent means. <br /><br />These students had allegedly paid others to take entrance exams on their behalf and their parents allegedly paid bribes to senior officials of the Madhya Pradesh Professional Examination Board to secure admissions in medical courses.<br /><br />Some of the students who managed to gain admission to government medical colleges had not even passed their class 12 exams, the probe had revealed. <br /><br />The Opposition Congress has targeted Chief Minister Shivraj Singh Chouhan over the issue and accused him of being directly involved with the scam. <br /><br />Though the Chief Minister was aware of the scam from 2006, he took no action against those involved; instead, he tried to cover up the wrong-doing of his government, the Congress has alleged. <br /><br />The state Director General of Police had earlier instructed the superintendents of police and inspector generals to arrest all those involved in the scam. The STF has been given a deadline of June 30 to arrest all the 400 suspects named in its chargesheet. <br /><br />The Congress has demanded the Chief Ministers resignation by that date, and threatened to launch a state-wide dharna if he refuses to step down. <br /><br />Former technical education minister Lakshmikant Sharma recently resigned from the primary membership of the BJP after he was arrested in connection with the scam earlier. <br /><br/>' +
              '</div><div></div><b><a class="place" href="http://www.ndtv.com/article/list/cities/1/bhopal" >Bhopal</a>:&nbsp;</b>The Special Task Force of the Madhya Pradesh Police has arrested 50 people -- including, students, parents and alleged brokers -- in connection with the multi-crore' +
              'Madhya Pradesh Professional Examination Board scam.<br /><br />Earlier in the day, the STF had conducted raids across the state and detained over 100' +
              'people over the education scam which initially surfaced in 2012-2013.<br /><br />The state authorities were forced to cancel the admission of over 1,000 students after it was revealed that they had gained admission to government medical colleges by fraudulent means. <br /><br />These students had allegedly paid others to take entrance exams on their behalf and their parents allegedly paid bribes to senior officials of the Madhya Pradesh Professional Examination Board to secure admissions in medical courses.<br /><br />Some of the students who managed to gain admission to government medical colleges had not even passed their class 12 exams, the probe had revealed. <br /><br />The Opposition Congress has targeted Chief Minister Shivraj Singh Chouhan over the issue and accused him of being directly involved with the scam. <br /><br />Though the Chief Minister was aware of the scam from 2006, he took no action against those involved; instead, he tried to cover up the wrong-doing of his government, the Congress has alleged. <br /><br />The state Director General of Police had earlier instructed the superintendents of police and inspector generals to arrest all those involved in the scam. The STF has been given a deadline of June 30 to arrest all the 400 suspects named in its chargesheet. <br /><br />The Congress has demanded the Chief Ministers resignation by that date, and threatened to launch a state-wide dharna if he refuses to step down. <br /><br />Former technical education minister Lakshmikant Sharma recently resigned from the primary membership of the BJP after he was arrested in connection with the scam earlier. <br /><br/>' +
              '</div>',
            'newsId': 231231
          },
          {
            'headline': 'This is news two headline',
            'description': 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum',
            'newsImageURL': '/images/rupeesfiscal.jpg',
            'content': '<div></div><b><a class="place" href="http://www.ndtv.com/article/list/cities/1/bhopal" >Bhopal</a>:&nbsp;</b>The Special Task Force of the Madhya Pradesh Police has arrested 50 people -- including, students, parents and alleged brokers -- in connection with the multi-crore' +
              'Madhya Pradesh Professional Examination Board scam.<br /><br />Earlier in the day, the STF had conducted raids across the state and detained over 100' +
              'people over the education scam which initially surfaced in 2012-2013.<br /><br />The state authorities were forced to cancel the admission of over 1,000 students after it was revealed that they had gained admission to government medical colleges by fraudulent means. <br /><br />These students had allegedly paid others to take entrance exams on their behalf and their parents allegedly paid bribes to senior officials of the Madhya Pradesh Professional Examination Board to secure admissions in medical courses.<br /><br />Some of the students who managed to gain admission to government medical colleges had not even passed their class 12 exams, the probe had revealed. <br /><br />The Opposition Congress has targeted Chief Minister Shivraj Singh Chouhan over the issue and accused him of being directly involved with the scam. <br /><br />Though the Chief Minister was aware of the scam from 2006, he took no action against those involved; instead, he tried to cover up the wrong-doing of his government, the Congress has alleged. <br /><br />The state Director General of Police had earlier instructed the superintendents of police and inspector generals to arrest all those involved in the scam. The STF has been given a deadline of June 30 to arrest all the 400 suspects named in its chargesheet. <br /><br />The Congress has demanded the Chief Ministers resignation by that date, and threatened to launch a state-wide dharna if he refuses to step down. <br /><br />Former technical education minister Lakshmikant Sharma recently resigned from the primary membership of the BJP after he was arrested in connection with the scam earlier. <br /><br/>' +
              '</div><div></div><b><a class="place" href="http://www.ndtv.com/article/list/cities/1/bhopal" >Bhopal</a>:&nbsp;</b>The Special Task Force of the Madhya Pradesh Police has arrested 50 people -- including, students, parents and alleged brokers -- in connection with the multi-crore' +
              'Madhya Pradesh Professional Examination Board scam.<br /><br />Earlier in the day, the STF had conducted raids across the state and detained over 100' +
              'people over the education scam which initially surfaced in 2012-2013.<br /><br />The state authorities were forced to cancel the admission of over 1,000 students after it was revealed that they had gained admission to government medical colleges by fraudulent means. <br /><br />These students had allegedly paid others to take entrance exams on their behalf and their parents allegedly paid bribes to senior officials of the Madhya Pradesh Professional Examination Board to secure admissions in medical courses.<br /><br />Some of the students who managed to gain admission to government medical colleges had not even passed their class 12 exams, the probe had revealed. <br /><br />The Opposition Congress has targeted Chief Minister Shivraj Singh Chouhan over the issue and accused him of being directly involved with the scam. <br /><br />Though the Chief Minister was aware of the scam from 2006, he took no action against those involved; instead, he tried to cover up the wrong-doing of his government, the Congress has alleged. <br /><br />The state Director General of Police had earlier instructed the superintendents of police and inspector generals to arrest all those involved in the scam. The STF has been given a deadline of June 30 to arrest all the 400 suspects named in its chargesheet. <br /><br />The Congress has demanded the Chief Ministers resignation by that date, and threatened to launch a state-wide dharna if he refuses to step down. <br /><br />Former technical education minister Lakshmikant Sharma recently resigned from the primary membership of the BJP after he was arrested in connection with the scam earlier. <br /><br/>' +
              '</div>',
            'newsId': 231233221
          }
        ]
      });
      //this.getNews();
    },
    render: function (){
      var newsItems;
      if(this.state.newsItems.length){
       newsItems = this.state.newsItems.map(function(item, index){
          return <NewsItem item={item} key={index} />
        });
      } else {
        newsItems = <h1> No News found for today! </h1>
      }


			return (
				<div>
          {newsItems}
				</div>
				)
		}		
	});

	return News;
});
