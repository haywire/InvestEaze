define(["react", "jsx!components/fundhouse/searchresult"], function(React, SearchResult){

var SearchList=React.createClass({


getInitialState: function(){
  
return{
  pageNo:1
  }
},

handleNextClick: function(pageNo){
this.setState({
  pageNo:this.state.pageNo+1
})
scroll(0,0)
},

handlePreviousClick: function(pageNo){
  this.setState({
    pageNo:this.state.pageNo-1
  })
},


render:function(){
      console.log('PortfolioType1='+this.props.PortfolioType1)
      console.log('PortfolioType2='+this.props.PortfolioType2)
        var rows = [];
        var page_rows=[];
        this.props.data.forEach(function(searchResult) {

            console.log(rows.length);

            if (!this.props.PortfolioType2 && !this.props.PortfolioType1) {
               rows.push(<SearchResult searchResult={searchResult} />);
               
            }

            if (searchResult.type==1 && this.props.PortfolioType1) {
               rows.push(<SearchResult searchResult={searchResult} />);
             
             }  

            if (searchResult.type==2 && this.props.PortfolioType2) {
               rows.push(<SearchResult searchResult={searchResult} />);
               
            }
            
            page_rows=rows.slice((this.state.pageNo-1)*10, this.state.pageNo*10);

          }.bind(this));

    
    return (<div>
            <p><strong>{rows.length}</strong> results found</p>
            <div>{page_rows}</div>
            <div id="pagination-center">
                <button type="button" className="btn btn-default" onClick={this.handlePreviousClick}>Previous</button>
                <button type="button" className="btn btn-default pull-right" onClick={this.handleNextClick} >Next</button>
            </div>
            </div>

            /**page_no={this.state.page_no}
            onPageChange={this.handlePageChange}/>
            </div>*/)

  }
      
     /** var searchNodes = this.props.data.map(function (searchResult) {
      return <SearchResult name={searchResult.name} Sharpe={searchResult.Sharpe} risk={searchResult.risk} type={searchResult.type}/>;
    });
    
      return (
      <div>
      {searchNodes}
      </div>
      )*/

  }
);

return SearchList;
})
