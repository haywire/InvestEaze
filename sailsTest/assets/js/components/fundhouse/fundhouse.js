define(["react", "jsx!components/fundhouse/main", "jsx!components/fundhouse/tabs", "jsx!components/fundhouse/backers"], function(React, Main, Tabs, Backers) {

	var Fundhouse=React.createClass({

  render: function(){
    return(

    	<div className="container-fluid" style={{"margin-top":"50px", "background-color":"#fff"}}>
      		<Main />
      		<Tabs />
      		<Backers />
       	</div>
    )
  }

})


return Fundhouse;

})