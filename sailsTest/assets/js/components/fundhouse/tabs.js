define(["react", "jsx!components/fundhouse/searchpage", "starrating", "carousel"], function(React, SearchPage, StarRating, Carousel) {


var Tabs=React.createClass({

componentDidMount:function(){
$(document).ready(function(){
  $("#slide-img").owlCarousel({

            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            autoPlay:true

            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false
});
        
$(".exemple2").jRating({
    type:'small', // type of the rate.. can be set to 'small' or 'big'
    length : 5, // nb of stars
    decimalLength : 5,
    showRateInfo:false // number of decimal in the rate
  })
  })
},


setActive: function(e) {
      this.setState({activeTab: e.target.innerHTML});
    },
    getInitialState: function() {
      return {
        activeTab: 'Overview'
      };
    },
    getDefaultProps: function() {
      return {
        activeTabPane: "tab-pane active"
      }
    },



  render: function(){
    return(
      <div className="main col-md-offset-1 col-md-7">
        <ul className="nav nav-tabs" >


        <li className={ this.state.activeTab == 'Overview' ? "active" : "" }><a className="pointer" onClick={this.setActive} >Overview</a></li>
            
            <li className={ this.state.activeTab == 'Activity' ? "active" : "" }><a className="pointer" onClick={this.setActive} >Activity</a></li>
            
            <li className={ this.state.activeTab == 'Funds' ? "active" : "" }><a className="pointer" onClick={this.setActive}>Funds</a></li>
            
            <li className={ this.state.activeTab == 'News & Press' ? "active" : "" }><a className="pointer" onClick={this.setActive}>News n Press</a></li>
      

      
        </ul>

        <div className="tab-content">
        <div className={ this.state.activeTab == 'Overview' ? this.props.activeTabPane : "tab-pane" } >

            <h2> The Pitch </h2>
                <div className="row well well-sm" style={{"font-size": "130%"}}>
                    Description dummy text description dummy text description dummy text description dummy
                    text description dummy text description dummy text.
                    dfs
                    <div id="slide-img" className="owl-carousel owl-theme">
                        <div className="item"><img src="images/gallery/image2.jpg" alt=""/></div>
                        <div className="item"><img src="images/gallery/image3.jpg" alt=""/></div>
                        <div className="item"><img src="images/gallery/image4.jpg" alt=""/></div>
                    </div>
                </div>
            <h2> The Team </h2>
        
                <div  className="row well well-sm">
                  <div className="col-md-4">
                   <div className="team-member-image">
                      <img src="http://img.pandawhale.com/6R3nGt-random-user-generator-yBUA.jpeg" className="img-responsive" /> 
                    </div>
                    <div>
                      <h4>Member Name</h4>
                      Desion dummy text description dummy text descripdsf tion dummy text description dummy
                    text description dumfmy text dsfdsfdescr dsiption dummy text. 
                    </div>
                  
                  </div>
                  <div className="col-md-4"><div className="team-member-image">
                      <img src="http://img.pandawhale.com/6R3nGt-random-user-generator-yBUA.jpeg" className="img-responsive" /> 
                    </div>
                    <div>
                      <h4>Member Name</h4>
                       Description dummy text description dummy text descripdsf tion dummy text description dummy
                    text description dumfmy text dsfdsfdescr dsiption dummy text. 
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="team-member-image">
                      <img src="http://img.pandawhale.com/6R3nGt-random-user-generator-yBUA.jpeg" className="img-responsive" /> 
                    </div>
                    <div>
                      <h4>Member Name</h4>
                     Description dummy text description dummy text descripdsf tion dummy text description dummy
                    text description dumfmy text dsfdsfdescr dsiption dummy text. 
                    </div>
                  </div>
                </div>
            
            <h2> Q n A</h2>
                <div className="row well well-sm" style={{"font-size": "130%"}}>
                 Description dummy text description dummy text defdsscription dummy text description dummy
                    text description safsad text description ddsfummy text.
                </div>
            <h2> Reviews</h2>
                <div className="row well well-sm" style={{"font-size": "100%"}}>
                  <div  >
                    <img className="reviewer-image col-md-2"  src="http://img.pandawhale.com/6R3nGt-random-user-generator-yBUA.jpeg" />
                  </div>
                 <div>

                  <h5> Name: User</h5>
                 </div>   
                 <div className="exemple2" data-average="10" data-id="2"></div>                  
                                    
                  <br />
                  In many ways Larsen presents her female characters as Romantic heroines trapped in a Naturalist novel. 
                  As the poet W.B. Yeats has lyrically expressed, they’re “sick with desire and fastened to a dying animal.” 
                  That dying animal is embodied in many ways in "Quicksand" and "Passing," from sterile or racist 
                  environments (such as Naxos and Clares home life with Bellew), 
                  to the fragile limitations of the female body, to the institutions of marriage and the 
              </div>

            </div>

        <div className={ this.state.activeTab == 'Activity' ? this.props.activeTabPane : "tab-pane" } >Activity Stuf</div>
        <div className={ this.state.activeTab == 'Funds' ? this.props.activeTabPane : "tab-pane" } ><SearchPage /></div>
        <div className={ this.state.activeTab == 'New & Press' ? this.props.activeTabPane : "tab-pane" } >Research Stuf</div>
    </div>
    </div>



      )


  }
})

return Tabs;
})