define(["react", "jsx!components/fundhouse/slider1", "jsx!components/fundhouse/slider2"], function(React, Slider1, Slider2){

var Sidebar=React.createClass({

  handleChange:function(){
    
    this.props.onUserInput(
        this.refs.Type1Input.getDOMNode().checked,
        this.refs.Type2Input.getDOMNode().checked

    );

  },




  render:function(){

    return (

    <div>
        <dl>
        <div style={{"font-size":"90%"}}>
          <dt className="" ><a href="#">Filter Results</a></dt>
           <dt><hr style={{"color": "#222" ,"background":"#222", "width": "100%", "height": "1px", "margin": "6px 0"}}></hr></dt>
                <dt><a href="#">Portfolio Type</a></dt>

                <div>
                  <dt><label><input type="checkbox" name="checkbox" value={this.props.PortfolioType1} ref="Type1Input" onChange={this.handleChange} /> Type 1</label></dt>
                  <dt><label><input type="checkbox" name="checkbox" value={this.props.PortfolioType2} ref="Type2Input" onChange={this.handleChange} /> Type 2</label></dt>
                </div>

                <div>
                  <hr style={{"color": "#222" ,"background":"#222", "width": "100%", "height": "1px", "margin": "6px 0"}}></hr>
                  <dt><a href="#">Asset Class</a></dt>
                  <dt><label><input type="checkbox" id="checkbox" value="value"/> Type 1</label></dt>
                  <dt><label><input type="checkbox" id="checkbox" value="value"/> Type 2</label></dt>
                  <dt><label><input type="checkbox" name="checkbox" value="value"/> Type 3</label></dt>
                  <dt><label><input type="checkbox" name="checkbox" value="value"/> Type 4</label></dt>
                  <hr style={{"color": "#222" ,"background":"#222", "width": "100%", "height": "1px", "margin": "6px 0"}}></hr>
                </div>
                <div>
                  <dt><label><input type="checkbox" id="checkbox" value="value"/>Only Investors followed</label>
                  <hr style={{"color": "#222" ,"background":"#222", "width": "100%", "height": "1px", "margin": "6px 0"}}></hr>
                  </dt>
                </div>

                <div>
                <dt><p>Risk    :<Slider1 /></p></dt>
                <dt><p>Days left:<Slider2 /></p></dt>
                </div>
        </div>
        </dl>
                     
  </div>
  );
  }
});

return Sidebar;
})

