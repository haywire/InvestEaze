define(["react"], function(React){

var SearchResult=React.createClass({

  render:function(){
    
   return(
      <div className="borderBox col-md-12">
        <article className="row">
          <div className="col-md-3">
            <a href="#" title="Lorem ipsum" className="img-thumbnail"><img src="http://www.therabbitjournal.com/journal/media/1/20060326-lucy%20small%20Thumbnail%20Web%20view.jpg" className="img-responsive" alt="Responsive image" /></a>
          </div>
          <div className="Fund-row col-md-2">
            <dl>
              <dt><span style={{"font-size":"120%"}}>{this.props.searchResult.name}</span></dt>
              <dt><small>50% invested</small></dt>
              <dt><small>$ 3000</small></dt>
            </dl>
          </div>
          <div className="Fund-row col-md-2">
            <dl>
              <dt><span>Sharps ratio </span></dt>
              <dt><span><small>{this.props.searchResult.Sharpe}</small></span></dt>
            </dl>
          </div>
          <div className="Fund-row col-md-2">
            <dl>
              <dt><span>Performance</span></dt>
              <dt><a href="#" title="Lorem ipsum" className="img-thumbnail col-md-11"><img 
              src="http://blog.socialcast.com/wp-content/uploads/2012/12/graph-20going-20up-small-300x200.jpeg" className="img-responsive" alt="Responsive image" /></a>
              </dt>
            </dl>
          </div>
          <div className="Fund-row col-md-3">
            <dl>
              <dt><span>See Details</span></dt>
              <dt><small>Risk: {this.props.searchResult.risk}</small></dt>
              <dt><small>Portfolio Type: {this.props.searchResult.type}</small></dt>
            </dl>
          </div>
      
          
        </article>
      </div>



      )
  }

})

return SearchResult;
})
