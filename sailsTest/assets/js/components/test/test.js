define(["react"], function(React) {

	var Test = React.createClass({
		getInitialState: function () {
			return {message: "Start"};
		},
		render: function () {
			return (
				<div>
				<div>{this.state.message}</div>
				<button onClick={this.doWork}>Solve</button>
				</div>
				)
		},
		doWork: function () {
			this.setState({message: "Loading..."});
			this.forceUpdate();
			var toDo = function() {
				for (var i = 0; i < 1000000000; i++);
			}
			toDo();
			this.setState({message: "Done."});
		}
	});

	return Test;

})