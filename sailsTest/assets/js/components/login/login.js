define(["react"], function(React) {

  var Login = React.createClass({
    getInitialState: function() {
      return {email: '', password: ''};
    },
    getDefaultProps: function() {
      return { screenHeight: $(window).height() }
    },
    handleEmailChange: function(e) {
      this.setState({email: e.target.value});
    },
    handlePasswordChange: function(e) {
      this.setState({password: e.target.value});
    },
    handleLogin : function() {
      console.log(this.state.email, this.state.password);
    },
    handleAuth: function(e) {
      var target = e.target;
      while(target.nodeName != "A")
        target = target.parentNode;
      var signinWin;
      var provider = "/auth/" + target.getAttribute("data-provider");
      signinWin = window.open(provider, "SignIn", "width=780,height=410,toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0");
      signinWin.focus();
      var loginCheck = setInterval(function() {
        if(signinWin.closed) {
          clearInterval(loginCheck);
          this.redirectLogin();
        }
      }.bind(this), 1000);
      return false;
    },
    redirectLogin: function() {
      $.get( 
       "/auth_user",
        function(data) {
          if(data.login) {

            this.props.setupUser(data.userId);
            window.location = "#/home"
          }
        }.bind(this)
      );
    },
    componentWillMount : function() {

      /*
      * @Author: Sambhav
      * Test Code for Message Controller (Chat feature)
      */

      /*
      * @Author: Yusuf
      * Commenting out socket code to save humanity.
      *

      this.props.socket = io.connect('http://localhost:1337');

      var socket = this.props.socket;
      var userId = '537b55a2d38701d0ef685019'; // Fetch this userId from session
      var recipientId = '53787fd7dcee29a03f0aee7c'; // Fetch this recipient Id from chatbox ID

      //Open a socket to the server for messages, we provide userId with the request
      socket.get('/message/conn', {userId: userId});

      socket.get('/message/conn', {userId: recipientId});

      var textMessage = "This is a hard coded test message"; // Fetch this message from message/chat text box

      /*
      * Pusher test code..
      */
      /*
      var pusher = new Pusher('4e38c8c41b8060f18b3e');
      var channel = pusher.subscribe(userId+'newNotification');
      channel.bind('newNotification', function(data) {
        console.log("Notification Received"+JSON.stringify(data));
      });

      var messageChannel = pusher.subscribe(userId+'newMessage');
      channel.bind('newMessage', function(data) {
        console.log("Message Received"+JSON.stringify(data));
      });

      var feedChannel = pusher.subscribe(userId+'newFeed');
      channel.bind('newNotification', function(data) {
        console.log("New Feed Received: "+JSON.stringify(data));
      }); */

      /*socket.get('/message/send',  
        {
          from: userId, 
          to: recipientId, 
          content: textMessage, 
          archived: false, 
          receiverRead: false, 
          receiverArchived: false});

        socket.on('messageReceived',
        function(messageDetails){
          console.log("Received: "+JSON.stringify(messageDetails));
        }
      );

      socket.get('/message/delete', 
        {
          user: recipientId,
          id: "537f2f5915a316fa25bcf32c",
          isReceiver : true
        }
      );

      socket.get('/message/delete', 
        {
          user: userId,
          id: "537f2f5915a316fa25bcf32c"
        }
      );

       socket.get('/message/read', 
        {
          id: "537f2f5915a316fa25bcf32c"
        }
      );*/

      /*
      * End of Test Code for Message Controller (Chat feature)
      */

    },

    render : function() {      
      return (
        <div className="row-fluid">
          <div className="loginImg col-md-6" style={{"height" : this.props.screenHeight + 'px'}}>
            
          </div>
          <div className="col-md-6" style={{"height" : this.props.screenHeight + 'px'}}>
            <table className="loginTable">
              <form className="form form-inline" action="#">
                <tr className="loginFormRow">
                  <td><input type="email" className="form-control" placeholder="Email" onChange={this.handleEmailChange} /></td>
                  <td><input type="password" className="form-control" name="password" placeholder="Password" onChange={this.handlePasswordChange} /></td>
                  <td><button type="submit" className="btn btn-info" onClick={this.handleLogin}>Login</button></td>
                </tr>
                <tr>
                  <td><span className="someClass"><input type="checkbox" /> Remember me</span></td>
                  <td><a href="#">Forgot Password ?</a></td>
                </tr>
              </form> 
              <tr className="loginFormRow">
                <td colSpan="2"><a data-provider="linkedin" onClick={this.handleAuth} className="btn btn-primary btn-block"><span className="authSign"><i className="fa fa-linkedin"></i> Sign Up with LinkedIn</span></a></td>
              </tr>
              <tr>
                <td colSpan="2"><a data-provider="facebook" onClick={this.handleAuth} className="btn btn-primary btn-block"><span className="authSign"><i className="fa fa-facebook"></i> Sign Up with Facebook</span></a></td>
              </tr>
              <tr>
                <td colSpan="2"><a data-provider="twitter" onClick={this.handleAuth} className="btn btn-primary btn-block"><span className="authSign"><i className="fa fa-twitter"></i> Sign Up with Twitter</span></a></td>
              </tr>
            </table>
            <span>By signing up you indicate that you have<br />read and agree to the <a href="#">Terms of Service.</a></span>
            <span className="loginBottom"><a href="#">About</a><a href="#">Careers</a><a href="#">Privacy</a><a href="#">Terms</a><a href="#">Directory</a></span>
          </div>
        </div>        
      );
    }
  })

  return Login;

})