
define(["react"], function(React) {

  var Metrics = React.createClass({
    render: function() {
      return (
        <div className="row">
          <div className="col-md-3">
            <div className="well well-sm text-center">
              <h4>Risk Score</h4>
              <h4>4/20</h4>
            </div>
          </div>
          <div className="col-md-3">
            <div className="well well-sm text-center">
              <h4>Performance</h4>
              <h4>4/20</h4>
            </div>
          </div>
          <div className="col-md-3">
            <div className="well well-sm text-center">
              <h4>Monthly vs S&amp;P</h4>
              <h4>4/20</h4>
            </div>
          </div>
          <div className="col-md-3">
            <div className="well well-sm text-center">
              <h4>Contact</h4>
              <h4>
                <span className="glyphicon glyphicon-bullhorn"></span>
                <span className="glyphicon glyphicon-home marginl10"></span>
                <span className="glyphicon glyphicon-camera marginl10"></span>
                <span className="glyphicon glyphicon-phone marginl10"></span>
              </h4>
            </div>
          </div>
        </div>
      );
    }
  })

  return Metrics;

})