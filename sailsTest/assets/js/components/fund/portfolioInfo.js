
define(["react"], function(React) {

  var PortfolioInfo = React.createClass({
    setActive: function(e) {
      this.setState({activeTab: e.target.innerHTML});
    },
    getInitialState: function() {
      return {
        activeTab: 'Summary'
      };
    },
    getDefaultProps: function() {
      return {
        activeTabPane: "tab-pane active well"
      }
    },
    render: function() {
      return (
        <div>
          <ul className="nav nav-tabs">
            <li className={ this.state.activeTab == 'Summary' ? "active" : "" }><a className="pointer" onClick={this.setActive} >Summary</a></li>
            <li className={ this.state.activeTab == 'Approach' ? "active" : "" }><a className="pointer" onClick={this.setActive} >Approach</a></li>
            <li className={ this.state.activeTab == 'Research' ? "active" : "" }><a className="pointer" onClick={this.setActive}>Research</a></li>
            <li className={ this.state.activeTab == 'Allocation' ? "active" : "" }><a className="pointer" onClick={this.setActive}>Allocation</a></li>
            <li className={ this.state.activeTab == 'Sell Discipline' ? "active" : "" }><a className="pointer" onClick={this.setActive}>Sell Discipline</a></li>
            <li className={ this.state.activeTab == 'Exceptions' ? "active" : "" }><a className="pointer" onClick={this.setActive}>Exceptions</a></li>
            <li className={ this.state.activeTab == 'Newsletters' ? "active" : "" }><a className="pointer" onClick={this.setActive}>Newsletters</a></li>
          </ul>
          <div className="tab-content">
            <div className={ this.state.activeTab == 'Summary' ? this.props.activeTabPane : "tab-pane" } >
              <p>Your event handlers will be passed instances of SyntheticEvent, a cross-browser wrapper around the browser's native event. It has the same interface as the browser's native event, including stopPropagation() and preventDefault(), except the events work identically across all browsers.</p>
              <p>React is a JavaScript library for creating user interfaces by Facebook and Instagram. Many people choose to think of React as the V in MVC. We built React to solve one problem: building large applications with data that changes over time. To do this, React uses two main ideas.</p>
            </div>
            <div className={ this.state.activeTab == 'Approach' ? this.props.activeTabPane : "tab-pane" } >Approach Stuff</div>
            <div className={ this.state.activeTab == 'Research' ? this.props.activeTabPane : "tab-pane" } >Research Stuf</div>
            <div className={ this.state.activeTab == 'Allocation' ? this.props.activeTabPane : "tab-pane" } >Allocation Stuff</div>
            <div className={ this.state.activeTab == 'Sell Discipline' ? this.props.activeTabPane : "tab-pane" } >SD Stuff</div>
            <div className={ this.state.activeTab == 'Exceptions' ? this.props.activeTabPane : "tab-pane" } >Exceptions</div>
            <div className={ this.state.activeTab == 'Newsletters' ? this.props.activeTabPane : "tab-pane" } >News Stuff</div>
          </div>
          <ul className="nav nav-pills">
            <li><button className="btn btn-info">Private</button></li>
            <li>
              <h4><span className="label label-warning marginl10">40% Committed</span></h4>
            </li>
            <li>
              <h4><span className="label label-success marginl10">25/1000 Raised</span></h4>
            </li>
          </ul>
        </div>
      );
    }
  });

  return PortfolioInfo;

})