
define(["react"], function(React) {

  var Holdings = React.createClass({
    render: function() {
      return (
        <div className="well wel-sm">
          <h4>Top 5 Holdings<small className="pull-right"><a href="#">View</a></small></h4>
          <table className="table table-condensed">
            <colgroup>
              <col style={{"width": "20%;"}} />
              <col style={{"width": "80%;"}} />              
            </colgroup>
            <tbody>
              <tr>
                <td>22.3%</td>
                <td>
                  <div className="progress">
                    <div className="progress-bar progress-bar-success" style={{"width": "90%"}}></div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>21.0%</td>
                <td>
                  <div className="progress">
                    <div className="progress-bar progress-bar-info" style={{"width": "85%"}}></div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>20.6%</td>
                <td>
                  <div className="progress">
                    <div className="progress-bar progress-bar-value" style={{"width": "83%"}}></div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>10.7%</td>
                <td>
                  <div className="progress">
                    <div className="progress-bar progress-bar-danger" style={{"width": "50%"}}></div>
                  </div>
                </td>
              </tr>
              <tr>
                <td>5.7%</td>
                <td>
                  <div className="progress progress-striped">
                    <div className="progress-bar progress-bar-danger" style={{"width": "25%"}}></div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <table className="table table-condensed">
            <tbody>
              <tr>
                <td><div className="circleFilled bgGreen"></div>&nbsp;SWY</td>
                <td><div className="circleFilled bgSkyBlue"></div>&nbsp;STRZA</td>
              </tr>
              <tr>
                <td><div className="circleFilled bgDeepBlue"></div>&nbsp;KN</td>
                <td><div className="circleFilled bgRed"></div>&nbsp;KCG</td>
              </tr>
              <tr>
                <td><div className="circleFilled bgRed"></div>&nbsp;CTB</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  })

  return Holdings;

})