define(["react", "jsx!components/navbar/navbar", "jsx!components/social/social", 'jsx!components/primary/primary', "models/user"], function (React, Navbar, Social, Primary, UserModel) {
  var MainComponent = React.createClass({
    getInitialState: function () {
      return {
        activeTab: 'Explore',
        activeSocialTab: 'ActivityFeed'
      }
    },

    getAuthUser: function(id){
      var that = this;
      var user = new UserModel();
      user.set({id: id});
      user.fetch({
          success: function(response) {
              that.setState({ authUser : response})
          }
      });
    },

    componentWillMount: function(){
      this.getAuthUser(this.props.user_id);
    },

    componentDidMount: function () {
      var that = this;
      this.forceUpdate();
    },
    updateStateOfComponent: function(state){
      console.log("main.js -> Updating State", state);
      this.setState({activeSocialTab: state});
      this.forceUpdate();
    },
    updatePrimarySectionState: function(state){
      console.log(state);
      this.setState({activeTab: state});
    },
    render: function () {
      var height = $(window).height() + 300;
      return (
        <div>
          <Navbar sendIdToParent={this.props.sendIdToParent} userId={this.props.user_id} updateTabState={this.updatePrimarySectionState} updateState={this.updateStateOfComponent}/>
          <div className='row-fluid'>
            <div className='col-md-8' style={{'background': '#fff', 'height': 'auto'}}>
              <Primary activeTab={this.state.activeTab} userId={this.props.user_id} />
            </div>
            <div className='col-md-4' style={{'height': height}}>
              <Social activeSection={this.state.activeSocialTab} userId={this.props.user_id} userObj={this.state.authUser}/>
            </div>
          </div>
        </div>)
    }
  });

  return MainComponent;
});