define(["react", "collections/notification", 'moment', "jsx!components/notification/notificationPanel"], function (React, NotificationCollection, moment, NotificationRow) {



  var Notification = React.createClass({
    showNotifications: function () {
      console.log("Notification Panel Showed");
      if ($('.notification-panel').css('display') == 'block') {
        $('.notification-panel').css({'display': 'none'})
      } else {
        $('.notification-panel').css({'display': 'block'})
      }
    },
    handleClick : function(e){
      this.props.updateState('Notifications');
    },
    getInitialState: function () {
      return {
        notifications: [],
        userId: this.props.userId,
        notificationInitialized: false
      }
    },
    getNotifications: function (id) {
      var that = this;
      var notification = new NotificationCollection();
      notification.fetchList(id, function (data) {
        that.setState({notifications: data});
      });
    },
    addNotificationToPanel: function (data) {
      console.log("New Notification Received .....", data);
      this.state.notifications.push(data);
      this.setState({ notifications: this.state.notifications});
    },
    bindPusherNotificationChannel: function () {
      window.puhserChannel.unbind('newNotification', this.addNotificationToPanel);
      window.puhserChannel.bind('newNotification', this.addNotificationToPanel);
    },
    initPushNotifications: function (id) {
      var that = this;
      if (!this.state.notificationInitialized) {
        console.log('Pusher Notification Initialized... for user :', id);
        if (window.pusher && window.pusherChannel) {
          this.bindPusherNotificationChannel(id);
        } else {
          window.pusher = new Pusher('1d21f8b62586ce0ac86f', { authEndpoint: '/auth/pusher' });
          window.puhserChannel = pusher.subscribe('presence-notificationChannel-' + String(id));
          window.pusher.connection.bind('error', function (err) {
            console.log(err);
          });
          this.bindPusherNotificationChannel(id);
        }
        this.state.notificationInitialized = true;
      }
    },
    componentWillMount: function () {
      /*Initial component bindings*/

    },
    componentDidMount: function(){
      console.log('Notification Count listener binded!');
      var that = this;
      $(document).on('notificationCountUpdate', function(e, data){
        console.log("Notification Count Updated", data);
        $('.notificationCount').html(data.notification.length || 0);
      });
    },
    componentWillReceiveProps: function (nextProps) {
      /*if(nextProps.userId){
        this.initPushNotifications(nextProps.userId);
        this.getNotifications(nextProps.userId);
      }*/
    },
    render: function () {

      return (
        <li className='notification-count'>
          <a href="javascript:void(0);" onClick={this.handleClick} className="dropdown-toggle info-number" data-toggle="dropdown">
            <i className="fa fa-bell-o"></i>
            <span className="badge notificationCount">{this.state.notifications.length}</span>
          </a>
        </li>
        );
    }
  });

  return Notification;
});