define(["react", "moment"], function(React, Moment) {

  var IndividualMessage=React.createClass({


      render: function(){
        var chatOrientation = String(this.props.message.get('from')) == String(this.props.userId) ? "in" : "out" ; 
        return(
            <li className= { chatOrientation }>
                <img src="images/user1.png" alt="" className="avatar" />
                <div className="message">
                    <span className="arrow"></span>
                    <a className="name" href="#">{ this.props.message.get('fromName')}</a>
                    <span className="datetime">{ Moment(this.props.message.get('createdAt')).fromNow() }</span>
                    <span className="body">
                        { this.props.message.get('content')}
                    </span>
                </div>
            </li>
        )
      }

    
  })

  return IndividualMessage;

})