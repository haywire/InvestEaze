define(["react", "moment"], function(React, Moment) {

  var ChatName=React.createClass({

      onChatNameClick: function(){
        var id=this.props.interaction.get('with');
        this.props.handleChatNameClick(id);
      },
      
      render: function(){
        return(
            <div className="row" >
            <span onClick={ this.onChatNameClick }>
              <div className="col-md-3">
                  <div className="avatar">
                      <img src="images/user1.png" alt="" />


                  </div>
              </div>
              <div className="col-md-5">
                  <h5>{this.props.interaction.get('withName')}</h5>
                  <span onClick={ this.onChatNameClick }>
                    <a href="#" className="small">{this.props.interaction.get('message')}</a>
                  </span>
              </div>
              <div className="col-md-4">
                  <a className="dir-like" href="#">
                      <span className="small">{Moment(this.props.interaction.get('timeStamp')).fromNow()}</span>
                      <i className="fa fa-clock-o"></i>
                  </a>
              </div>
              </span>
            </div>     
        )
      }

    
  })

  return ChatName;

})