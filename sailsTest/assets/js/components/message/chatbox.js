define(["react", "collections/message", "models/message", "models/user", "jsx!components/message/individualmessage"], function(React, MessageCollection, Message, User, IndividualMessage) {


  var ChatWindow=React.createClass({

        getInitialState:function()
        {//var id=this.props.activeUser
          //console.log(id)

          var id=this.props.activeUser;
          return{
            messageList:[],
            innerElement:"",
            user: {}
          }
        },

        setListFlag: function(){
          this.props.setListFlag()
        },

        getPushMessages: function(){
          console.log("pusher pusher pusher")
          var that = this;
          var pusher = new Pusher('1d21f8b62586ce0ac86f', { authEndpoint: '/auth/pusher' });
          var channel = pusher.subscribe('presence-messageChannel-' + String(this.props.userId));
          channel.bind('newMessage', function(data) {
            that.state.messageList.push(data)
            that.setState({ messageList : that.state.messageList})
          });
        },

        getMessages: function(id){
            var messageCollection = new MessageCollection();
                this.setState({ messageList : messageCollection});
                var messages = messageCollection.fetchCurrent(id, function(messages){
                  console.log(messages)
                    String(messages.models[0].get('from')) == String(this.props.userId) 
                      ? this.setState({ activeName : messages.models[0].get('toName'),
                                        messageList : messages}) 
                      : this.setState({ activeName : messages.models[0].get('fromName'),
                                        messageList: messages}) ;  
                }.bind(this)
            );
        },

        componentWillMount: function(){

        this.getMessages(this.props.activeUser);

        // Pusher code ahead
        this.getPushMessages();

        },

       componentWillReceiveProps: function(nextProps){
          this.getMessages(nextProps.activeUser);
         },

       sendMessages: function(){
        var messageModel = new Message();
        messageModel.save(
            {
              to : this.props.activeUser,
              from : this.props.userId,
              content :document.getElementById('inputField').value, 
            },
            {
                url: '/message',
                success: function(model, response, options) {
                    response.fromName = this.props.userObj.get('name');
                    this.state.messageList.push(response);
                    this.setState({messageList: this.state.messageList});
                 
                }.bind(this),
                error: function(resp) {
                  
                    /* POST COULD NOT BE UPDATED */
                }
            }
        )

        document.getElementById('inputField').value="";
        },

        render:function(){
          var that = this;
          var id=this.props.activeUser

          var messages=this.state.messageList.map(function(message){
             return  < IndividualMessage key={message.get('id')} message={message} userId={that.props.userId} />
          })
          var inputStyle = {
            width : '100%'
          };

          return(

          <div className="panel-body">
          <div className="row">
            <div className="col-md-10">
            <header className="panel-heading">
              <h3>Conversation with {this.state.activeName}</h3>
            </header>
            </div>
            <div className="col-md-2">
            <span className="fa fa-chevron-left avatar tools" onClick={ this.setListFlag }></span>
            </div>
            </div>
            <ul className="chats cool-chat">
              {messages}
            </ul>

            <div className="chat-form ">
              <div className="form-inline">
                  <div className="form-group">
                      <input id="inputField" type="text" style={inputStyle} placeholder="Type a message here..." className="form-control" />
                  </div>
                  <button className="btn btn-primary" onClick={this.sendMessages}>Send</button>
              </div>
          </div>
          </div>
          )
        }
       })

return ChatWindow;

})
