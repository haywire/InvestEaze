define(["react", "jsx!components/message/chatname"], function(React, ChatName){


var ChatList=React.createClass({
    getInitialState: function(){
          return{
          }
        },
        
        onChatNameClick:function(id){
           
          this.props.onUserNameClick(id)
                 
        },

        handleLoadClick: function(check){

          this.setState({
            check:this.state.check+2
          })
        },

        render: function(){

          var rows = [];

          this.props.data.forEach(function(interaction) {
                rows.push(<ChatName key={interaction.get('with')}
                        activeUser={this.props.activeUser}
                        handleChatNameClick={this.onChatNameClick}
                        interaction={interaction}/>)    
          }.bind(this));
          
          return(

                  <div className="panel">
                    <header className="panel-heading">
                            <h1>Messages</h1>
                    </header>
                    <div className="panel-body">
                      <div className="dir-info">
                      {rows}
                      </div>
                    </div>
                  </div>
          )
        }


      })


return ChatList;

})