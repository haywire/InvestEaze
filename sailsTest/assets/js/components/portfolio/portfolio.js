define(["react",  "bootstrap"], function (React, bootstrap) {
  var Portfolio = React.createClass({
    setActive: function(e) {
      this.setState({activeTab: e.target.innerHTML});
    },
      getInitialState: function() {
      return {
        postItems: [],
        activeTab: 'Projected Performance',


      };
    },
     getDefaultProps: function() {
      return {
        activeTabPane: "tab-pane active"
      }
    },
   
  

    dropdown: function(){
      var dropdown=document.getElementById("haha")
      dropdown.style.display=dropdown.style.display==='none'?'':'none';
      var caret=document.getElementById("caret")
        caret.className=(caret.className==='fa fa-chevron-down'?'fa fa-chevron-up':'fa fa-chevron-down');

     // $( "#haha" ).toggle( "blind");

    },

    componentDidMount: function(){
      Morris.Line({
  element: 'line-example',
  data: [
    { y: '2000', a: 10, b: 9 },
    { y: '2001', a: 50,  b: 40 },
    { y: '2002', a: 25,  b: 35 },
    { y: '2003', a: 50,  b: 40 },
    { y: '2004', a: 25,  b: 35 },
    { y: '2005', a: 75,  b: 65 },
    { y: '2006', a: 50,  b: 40 },
    { y: '2007', a: 50,  b: 40 },
    { y: '2008', a: 25,  b: 35 },
    { y: '2009', a: 65,  b: 65 },
    { y: '2010', a: 25,  b: 35 },
    { y: '2011', a: 50,  b: 40 },
    { y: '2012', a: 100, b: 90 }
  ],
  xkey: 'y',
  ykeys: ['a', 'b'],
  labels: ['Series A', 'Series B']
});
      Morris.Area({
  element: 'area-example',
  data: [
    { y: '2006', a: 100, b: 90 },
    { y: '2007', a: 75,  b: 65 },
    { y: '2008', a: 50,  b: 40 },
    { y: '2009', a: 75,  b: 65 },
    { y: '2010', a: 50,  b: 40 },
    { y: '2011', a: 75,  b: 65 },
    { y: '2012', a: 100, b: 90 }
  ],
  xkey: 'y',
  ykeys: ['a', 'b'],
  labels: ['Series A', 'Series B']
});
      Morris.Donut({
  element: 'donut-example',
  data: [
    {label: "Download Sales", value: 12},
    {label: "In-Store Sales", value: 30},
    {label: "Store Sales", value: 30},
    {label: "Out-Store Sales", value: 30},
    {label: "Mail-Order Sales", value: 20}
  ]
});

    $(".tooltip-examples p").tooltip({
        placement : 'right'
    });


     },
      
    render: function(){
    
    return(        

      <div className="row">
        <div className="col-md-3">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Goals</h3>
            </div>
            <div className="panel-body">
              <ul style={{"padding-right":"25px"}}>
                <strong><h4> Play the Market <span className="fa fa-chevron-right pull-right"></span> </h4></strong><br/>
                Amount<br />
                <input id="amountSlider" type="range" style={{"width":"80%"}}/>
                <br/>
                Time<br/>
                <input id="timeSlider" type="range" style={{"width":"80%"}} /><br/>
                <div className="portfolioLink tooltip-examples list-inline">
                  <p data-toggle="tooltip" data-original-title="Description goes here Description goes here Description goes here Description goes here">Education<br/>
                  <span className="light">Rs 5,00,000   5 years</span>
                  </p>
                </div> 
                <div className="portfolioLink tooltip-examples list-inline">
                  <p data-toggle="tooltip" data-original-title="Description goes here Description goes here Description goes here Description goes here">Marriage<br/>
                  <span className="light">Rs 5,00,000   5 years</span>
                  </p>
                </div> 
                <div className="portfolioLink tooltip-examples list-inline">
                  <p data-toggle="tooltip" data-original-title="Description goes here Description goes here Description goes here Description goes here">Child Education<br/>
                  <span className="light">Rs 5,00,000   5 years</span>
                  </p>
                </div> 
                <div className="portfolioLink tooltip-examples list-inline">
                  <p data-toggle="tooltip" data-original-title="Description goes here Description goes here Description goes here Description goes here">Retirement<br/>
                  <span className="light">Rs 5,00,000   5 years</span>
                  </p>
                </div> 
                <div className="portfolioLink tooltip-examples list-inline">
                  <p data-toggle="tooltip" data-original-title="Description goes here Description goes here Description goes here Description goes here">Marriage<br/>
                  <span className="light">Rs 5,00,000   5 years</span>
                  </p>
                </div> <br/>
                <button className="btn btn-block btn-default" type="button" style={{"margin-left":"-15px"}}><strong> + Add Your Plan </strong></button> 
              </ul>
            </div>
          </div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Past Investment</h3>
            </div>
            <div className="panel-body" >
              <ul style={{"padding-right":"25px"}}>
                <h5>Investment<span className="pull-right">Rs 30,00,000</span></h5>
                <h5>Profit<span className="pull-right">1,70,000</span></h5>
                <h5>Return % <span className="pull-right">40%</span></h5>
                <div id="haha" style={{"display": "none"}}>
                  <hr/>
                  <div className="">
                    <p>Child Education<br/>
                    <span className="light">Rs 5,00,000   5 years</span>
                    </p>
                  </div> 
                  <div className="">
                    <p>Child Education<br/>
                    <span className="light">Rs 5,00,000   5 years</span>
                    </p>
                  </div> 
                  <div className="">
                    <p>Child Education<br/>
                    <span className="light">Rs 5,00,000   5 years</span>
                    </p>
                  </div> 
                </div>
                <span id="caret" className="fa fa-chevron-down" onClick={this.dropdown} ></span>
              </ul>
            </div>
          </div>
        </div>
             

<div className="col-md-9">
  <div className="panel">
    <div className="panel-body">
      <div className="col-md-9" >
        <h3>Invest in Play the Market</h3>
        <span className="col-md-3">Amount: Rs 70,000 </span>
        <span className="col-md-3">Time:2 years</span>
      </div><br/>
      <div className="col-md-3" style={{"margin-top":"0px"}}><button className="btn btn-lg btn-warning">Invest Now</button></div>
      <div className="col-md-12" >
        <div className="col-md-4">
          <div id="donut-example"></div>
        </div>
        <div className="col-md-8" style={{"margin-top":"40px"}}>
         <table className="table">
                            <thead>
                            <tr>
                                
                                <td>Asset Class</td>
                                <td>Investment</td>
                                <td>%</td>
                                <td>Amount</td>
                              
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                          
                                <td>Crisil Rank</td>
                                <td></td>
                                <td></td>
                                <td></td>
                               
                                
                            </tr>
                            <tr>
                                
                                
                                <td>Crisil Rank</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            
                            </tr>
                            <tr>
                                
                             
                                <td>Crisil Rank</td>
                                <td></td>
                                <td></td>
                                <td></td>
                               
                            </tr>
                            <tr>
                                
                             
                                <td>Crisil Rank</td>
                                <td></td>
                                <td></td>
                                <td></td>
                               
                            </tr>
                             <tr>
                                
                             
                                <td>Crisil Rank</td>
                                <td></td>
                                <td></td>
                                <td></td>
                               
                            </tr>
                            </tbody>
                        </table>
            </div>
      </div>
    </div>
  </div><br/>
  <section className="panel">
    <header className="panel-heading custom-tab">
      <ul className="nav nav-tabs" >
        <li className={ this.state.activeTab == 'Projected Performace' ? "active" : "" }><a className="pointer" onClick={this.setActive} >Projected Performance</a></li>
        <li className={ this.state.activeTab == 'Historical Performance' ? "active" : "" }><a className="pointer" onClick={this.setActive} >Historical Performance</a></li>
        <li className={ this.state.activeTab == 'Your Costs' ? "active" : "" }><a className="pointer" onClick={this.setActive}>Your Costs</a></li>
      </ul>
    </header>
    <div className="panel-body">
      <div className="tab-content col-md-8">
        <div className={ this.state.activeTab == 'Projected Performance' ? this.props.activeTabPane : "tab-pane" } >
          <div id="line-example"></div>
        </div>
        <div className={ this.state.activeTab == 'Historical Performance' ? this.props.activeTabPane : "tab-pane" } >
         <div id="area-example"></div>
        </div>
        <div className={ this.state.activeTab == 'Your Costs' ? this.props.activeTabPane : "tab-pane" } >
          <br/>
          <h3> Infoassembly Fee </h3>
          <h4> Advisory Fee </h4>
          <br/>
          <h3> Third Party Fee </h3>
          <h4> ETF Expenses</h4>
          <h4> Commission </h4>
        </div>
      </div>
    </div>
  </section>
</div>
</div>



      ) 

}
  })
  return Portfolio;
});