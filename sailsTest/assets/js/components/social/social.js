define(["react", 'moment', 'jsx!components/notification/notificationPanel', 'jsx!components/message/message', 'jsx!components/feeds/userFeed', 'jsx!components/feeds/publicFeed'], function (React, moment, NotificationPanel, MessageSection, UserFeeds, PublicFeeds) {
  var Social = React.createClass({
    getInitialState: function () {
      return {
        activeSection: 'ActivityFeed',
        otherUser: ''
      }
    },
    componentWillMount: function(){
      this.currentAuthUserId = this.props.userId;
      console.log("Social.js -> Current State 1", this.props.activeSection);
      this.setState({activeSection: this.props.activeSection});
    },
    componentDidMount: function () {
      console.log("Social.js -> Current State 2", this.props.activeSection);
      this.setState({activeSection: this.props.activeSection});
    },
    componentWillReceiveProps: function (nextProps) {
      this.currentAuthUserId = nextProps.userId;
      console.log("Social.js -> Current State 3", nextProps.activeSection);
      this.setState({activeSection: nextProps.activeSection});
    },
    showUserProfile: function (id){
      console.log(id);
      this.setState({activeSection: 'UserFeeds', otherUser: id})
    },
    render: function () {
      console.log("Current Section to Show: ", this.state.activeSection);
      var height = $(window).height();
      var section;

      switch (this.state.activeSection) {
        case 'Notifications':
          section = <NotificationPanel userId={this.props.userId} showUserProfile={this.showUserProfile}/>
          break;
        case 'Messages':
          section = <MessageSection userId ={this.currentAuthUserId} userObj = {this.props.userObj} />
          break;
        case 'UserFeeds':
          section = <UserFeeds userId={this.currentAuthUserId} otherUser ={this.state.otherUser} showUserProfile={this.showUserProfile}/>
          break;
        case 'ActivityFeed':
          section = <PublicFeeds userId={this.props.userId} userObj = {this.props.userObj} showUserProfile={this.showUserProfile}/>
          break;
        default:
          section = <NotificationPanel userId={this.props.userId} showUserProfile={this.showUserProfile}/>
          break;
      }

      return (

        <div style={{"height": height}}>
          {section}
        </div>
        );
    }
  });
  return Social;
});