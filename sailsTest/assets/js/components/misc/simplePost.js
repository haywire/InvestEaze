define(['react', 'models/comment', 'models/post', 'collections/comment', 'models/activity'], function(React, CommentModel, PostModel, CommentCollection, Activity) {

	/* 
		Simple Post component contains only the following:
		Initiator name and image
		Post content contains only text
		Footer contains likes, shares etc.
	*/

	var simplePost = React.createClass({
		getInitialState: function() {
			return {
				comments: [],
				commentText: '',
				commentBoxVisible: false,
				likes:this.props.postData.get('numberOfLikes')
			}
		},

		like: function(initiator, id, initiatorId, flag) {
			var val = $('#'+id).text();
			$('#'+id).toggleClass('userLikes').text( $('#'+id).hasClass('userLikes') ? value="Unlike": value="Like"  );
			var like = new Activity();
			if (flag==1){
			if(val == 'Like'){
				this.setState({ likes : this.state.likes+1 })
			}
			else{
				this.setState({ likes : this.state.likes-1 })
			}
		}

        	var likeDetails = {
	            initiator : String(initiator),
	            parentId : String(id),
	            parentIdInitiator : String(initiatorId),       
	            parentCollection : 'Post',
	            activityType : val
        	};
        	like.save(likeDetails,{
            	success: function(like){
            }
        })
		},

		/*
		unlike : function() {
			var unlike = new Activity();
			console.log(this.props.userData.get('id'))
        	var likeDetails = {
	            initiator : String(this.props.userData.get('id')),
	            parentId  : String(this.props.postData.get('_id')),        
	            parentCollection : 'Post',
	            activityType : 'Like'
        	};
        	like.save(likeDetails,{
            	success: function(like){
                console.log(like);
            }
        })
		},
		*/


		comment: function(id) {
			/* Fetch all comments for this post */
			console.log(id)
			console.log("++++++++++")
			var comments = new CommentCollection();
			var that = this;
			comments.fetchCurrent(id, function(commentsFromServer){
				that.setState({comments: commentsFromServer, commentBoxVisible: true})
			})

				var div =document.getElementById("comments"+id)
				if (div.style.display!=='none'){
					div.style.display='none'
				}
				else{
					div.style.display='block'
				}

			/* Comments fetched */
			/* Render all comments */
		},

		share: function(id) {
			var val = $('#Share'+id).text();
			$('#Share'+id).addClass('userShares').text( $('#Share'+id).hasClass('userShares') ? value="Shared": value="Share"  );
			var postModel = new PostModel();
	        

	        postModel.save(
	            {
	                initiator: this.props.userData.get('id'),
	                postType: 'Post',
	                content: this.props.postData.get('content'),
	                sharedPostId : this.props.postData.get('_id'),
	                sharedPostInitiator : this.props.postData.get('initiator')
	            },
	            {
	                url: '/post',
	                success: function(model, response, options) {
	                    this.state.postItems.unshift(model);
	                    this.setState({postItmes: this.state.postItems, postText: ''});
	                 
	                }.bind(this),
	                error: function(resp) {
	                  
	                    /* POST COULD NOT BE UPDATED */
	                }
	            }
	        )
		},

		handleCommentChange: function(e) {
			this.setState({commentText: e.target.value})
		},

		sendComment: function(e) {
      if(e.keyCode == 13){
        var comment = new CommentModel();

        comment.save(
          {
            initiator: String(this.props.userData.get('id')),
            parentId: String(this.props.postData.get('_id') || this.props.postData.get('id')), // do check the issue out .. need to put _id or it wont work
            content: this.state.commentText,
            parentIdInitiator: String(this.props.postData.get('initiator'))
          },
          {
            success: function(model, response, options) {
              this.state.comments.push(response);
              this.setState({comments: this.state.comments, commentText: ''})
            }.bind(this)
          })
      }
		},
		convertDate: function(d) {
			d = new Date(d)
			var date = d.getDate();
			var months = ["ILoveChicken","January","February","March","April","May","June","July","August","September","October","November","December"];
			var hours = d.getHours();
			var mins = d.getMinutes();
			var pmflag = hours > 11;
			hours = hours > 12 ? (hours-12) : hours;
			var str = '' + months[d.getMonth()] + ' ' + date + ' at ' + hours + ':' + mins + (pmflag ? 'pm' : 'am')
			return str;
		},
		componentWillMount: function() {

		},
		render: function() {
			var postId= this.props.postData.get('_id') || this.props.postData.get('id');
			var CommentBoxClass = {
				'hide': !this.state.commentBoxVisible,
				'row': true
			}
			var comments = this.state.comments.map(function(comment) {

				commentId=comment.get('id') || comment.get('_id');

				return (
					<div className="row margint10" style={{'margin-right': '0px'}}>
            			<div className="col-md-12 borderBottom">
	            			<div className="media">
								<a className="pull-left" href="#">
									<img className="media-object" src={this.props.userData.get('imageLink')} className="commentImage avatari" />
								</a>
								<div className="media-body">
									<span className="media-heading commentInitiator">
										<a href="#">{this.props.userData.get('name')}</a>
									</span>
									<span className="commentContent">
										{comment.get('content')}
									</span><br />
									<span className="paddingr10">
										{this.convertDate(comment.get('updatedAt'))}
									</span>

									<span id={ commentId } className="pointer like" onClick={this.like.bind(null, this.props.userData.get('id'), commentId, comment.get('initiator'), 0)}>Like

									</span>
								</div>
							</div>
            			</div>
          			</div>
				)
			}.bind(this))
			return (
        <div className="panel" style={{"margin-bottom": "5px"}} >
          <div className="panel-body">
            <div className="dir-info">
              <div className="row" style={{'margin-right': '0'}}>
                <div className="col-xs-2">
                  <div className="avatar">
                    <a href='javascript:void(0);' onClick={this.props.showUserProfile.bind(null, this.props.userData.get('id'))}>
                      <img src={this.props.userData.get('imageLink')} style={{'border-radius': '0', '-webkit-border-radius': '0'}} alt=""/>
                    </a>
                  </div>
                </div>
                <div className="col-xs-7">
                  <div className='news-feed-name'> {this.props.postData.get('initiatorName') || this.props.userData.get('name')}</div>
                  <p className='news-feed-content'>{this.props.postData.get('content')}</p>
                </div>
                <div className="col-xs-3">
                  <a className="dir-like" href="javascript:void(0);">
                    <span className="small">{moment(this.props.postData.get('createdAt')).fromNow()}</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="panel-footer" style={{'margin-bottom': '6px'}}>
            <span id={postId} className="marginr10 pointer like" onClick={this.like.bind(null, this.props.userData.get('id'), postId, this.props.postData.get('initiator'),
              1)}>Like</span>

            <span className="marginr10 pointer" onClick={this.comment.bind(null,postId)}>Comment</span>
            <span id={'Share'+ String(postId)} className="marginr10 pointer" onClick={this.share.bind(null,postId)}>Share</span>

            <div className='pull-right'>
              <span className="fa fa-arrow-circle-up marginr10 pointer"> {this.state.likes}</span>
              <span className="fa fa-comment marginr10 pointer"> {this.props.postData.get('numberOfComments')}</span>
              <span className="fa fa-share marginr10 pointer"> {this.props.postData.get('numberOfShares')}</span>
			      </div>
			      <div id={"comments"+postId} style={{"display":"none"}}>
			                   {comments}

            <div className={React.addons.classSet(CommentBoxClass)}>
              <div className="col-md-12 margint10">
                <input className="form-control" onKeyUp={this.sendComment} placeholder="Post your comment" onChange={this.handleCommentChange} value={this.state.commentText} />
              </div>
            </div>
            </div>
          </div>
        </div>

			);
		}
	});
	return simplePost;

});