
define(['react'], function(React) {

  var Post = React.createClass({


   
    
    render: function() {

      return (
        <div className="panel" style={{"margin-bottom": "5px"}} >
          <div className="panel-body">
            <div className="dir-info">
              <div className="row" style={{'margin-right': '0'}}>
                <div className="col-xs-2">
                  <div className="avatar">
                    <a href='javascript:void(0);' onClick={this.props.showUserProfile.bind(null, this.props.data.initiator)}>
                      <img src='https://pbs.twimg.com/profile_images/2322714077/c4k3j6wpq88k84e62tag_400x400.jpeg' style={{'border-radius': '0', '-webkit-border-radius': '0'}} alt=""/>
                    </a>
                  </div>
                </div>
                <div className="col-xs-7">
                  <div className='news-feed-name'> {this.props.data.initiatorName || this.props.data.name}</div>
                  <p className='news-feed-content'>{this.props.data.initiatorName} {this.props.data.activityType} <br/>{this.props.data.content}</p>
                </div>
                <div className="col-xs-3">
                  <a className="dir-like" href="javascript:void(0);">
                    <span className="small">{moment(this.props.data.createdAt).fromNow()}</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="panel-footer" style={{'margin-bottom': '6px'}}>
          {/*<div className='pull-right'>
              <span className="fa fa-arrow-circle-up marginr10 pointer"> {this.props.data.numberOfLikes}</span>
              <span className="fa fa-comment marginr10 pointer"> {this.props.data.numberOfComments}</span>
              <span className="fa fa-share marginr10 pointer"> {this.props.data.numberOfShares}</span>
            </div>*/}
          </div>
        </div>
      );
    }
  });

  return Post;

});



                      