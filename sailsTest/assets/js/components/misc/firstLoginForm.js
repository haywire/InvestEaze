
define(["react", "models/user"], function(React, User) {

	var FormL = React.createClass({
		updateUser: function() {
			
		},
		componentWillMount: function() {
			console.log(this.props.user_id)
		},
		render: function() {
			var fields = [{"name":"Email"},{"name":"Organization Name"},{"name":"Street"},{"name":"House"},{"name":"City"},{"name":"State"},{"name":"Pin"},{"name":"Phone"},{"name":"Professional Title"},{"name":"Previous Companies"},{"name":"Tags"}];
			var controls = fields.map(function(field) {
				return (
					<div className="col-md-3" key={field.name}>
						<label className="control-label">{field.name}</label>
						<div>
							<input type="text" />
							<p className="help-block">Your {field.name} goes here</p>
						</div>
					</div>
					);
			});
			return (
				<div className="row-fluid">
					<div className="col-md-12">
						<form>
						  <fieldset>
						    <div id="legend">
						      <legend className="legend">First Time Login Info</legend>
						    </div>

						    {controls}

						    <div className="control-group">
						      <div className="controls">
						        <button className="btn btn-success" onClick={this.updateUser}>Register</button>
						      </div>
						    </div>
						  </fieldset>
						</form>
					</div>
				</div>
			)
		}
	})

	return FormL;

})