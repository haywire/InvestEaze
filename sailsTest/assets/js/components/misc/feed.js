define(["react", "jsx!components/misc/post", "models/user", "jsx!components/index/featuredpost", "collections/newsFeed"], function (React, Post, User, FeaturedPost, NewsFeed) {

  var Feed = React.createClass({

    getInitialState: function () {
      return {
        postItems: []
      }
    },


    fetchFeed: function () {
      var feed = new NewsFeed();
      var that = this;
      console.log(this.props.id);
      console.log("In the feed. From the mountain");
      feed.fetch({
        success: function (response) {
          that.setState({ postItems: response}, function () {
          });
        }
      })
    },

    componentWillMount: function () {
      this.fetchFeed();
      this.setState({postItems: [
        {
          "_id": "5398455c3456f102f7cdb780",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T12:02:36.692Z",
          "updatedAt": "2014-06-11T12:02:36.692Z"
        },
        {
          "_id": "539844138595dab6f5bd4f73",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T11:57:07.910Z",
          "updatedAt": "2014-06-11T11:57:07.910Z"
        },
        {
          "_id": "539841da984b3c2cf3ce3a29",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T11:47:38.061Z",
          "updatedAt": "2014-06-11T11:47:38.061Z"
        },
        {
          "_id": "53984096565b2ee8f166877d",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T11:42:14.930Z",
          "updatedAt": "2014-06-11T11:42:14.930Z"
        },
        {
          "_id": "5398408b565b2ee8f166877b",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "content": "Hello",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "numberOfLikes": 0,
          "createdAt": "2014-06-11T11:42:03.141Z",
          "updatedAt": "2014-06-11T11:42:03.141Z"
        },
        {
          "_id": "53983f62b8e3f54bf1934123",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T11:37:06.191Z",
          "updatedAt": "2014-06-11T11:37:06.191Z"
        },
        {
          "_id": "53983d5df1b376adee54d1cf",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T11:28:29.045Z",
          "updatedAt": "2014-06-11T11:28:29.045Z"
        },
        {
          "_id": "53983c1c666e4ed0edec06eb",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T11:23:08.335Z",
          "updatedAt": "2014-06-11T11:23:08.335Z"
        },
        {
          "_id": "53983b4453e5cd6fec233975",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T11:19:32.036Z",
          "updatedAt": "2014-06-11T11:19:32.036Z"
        },
        {
          "_id": "539837c40bbad9d0e9d6dbf1",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T11:04:36.075Z",
          "updatedAt": "2014-06-11T11:04:36.075Z"
        },
        {
          "_id": "539835406b5ff889e7c8e824",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5396ef2b135b34876cda044b",
          "parentCollection": "Post",
          "activityType": "Like",
          "parentIdInitiator": "5396d98739e3b24758372f15",
          "createdAt": "2014-06-11T10:53:52.279Z",
          "updatedAt": "2014-06-11T10:53:52.279Z"
        },
        {
          "_id": "5396ef2b135b34876cda044b",
          "initiator": "5396d98739e3b24758372f15",
          "postType": "Post",
          "content": "howtojs",
          "numberOfLikes": 10,
          "numberOfComments": 0,
          "numberOfShares": 0,
          "createdAt": "2014-06-10T11:42:35.368Z",
          "updatedAt": "2014-06-10T11:42:35.368Z"
        },
        {
          "_id": "5396eddbfedcfc706b4fb122",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5395463756bc862225213f56",
          "parentCollection": "User",
          "activityType": "Follow",
          "createdAt": "2014-06-10T11:36:59.429Z",
          "updatedAt": "2014-06-10T11:36:59.429Z"
        },
        {
          "_id": "5396ecab1891c23b6a5ee10a",
          "initiator": "5396d98739e3b24758372f15",
          "parentId": "5395463756bc862225213f56",
          "parentCollection": "User",
          "activityType": "Follow",
          "createdAt": "2014-06-10T11:31:55.880Z",
          "updatedAt": "2014-06-10T11:31:55.880Z"
        },
        {
          "_id": "5396d9e139e3b24758372f18",
          "initiator": "5396d98739e3b24758372f15",
          "postType": "Post",
          "content": "hello world\n",
          "numberOfLikes": 7,
          "numberOfComments": 0,
          "numberOfShares": 0,
          "createdAt": "2014-06-10T10:11:45.683Z",
          "updatedAt": "2014-06-10T10:11:45.683Z"
        }
      ]})
    },

    render: function () {
      var that = this;
      var postItems = this.state.postItems.map(function (post) {
        return <Post data={post} key={post.id} showUserProfile={that.props.showUserProfile}/>;
      });

      return (
        <div>
            {postItems}
        </div>
        )
    }
  })
  return Feed;
})
