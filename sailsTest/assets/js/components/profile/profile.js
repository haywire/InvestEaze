//profile.js
define(["react", "jsx!components/navbar/navbar", "jsx!components/misc/simplePost", "jsx!components/misc/post", "jsx!components/misc/feed", "jsx!components/profile/activity", "models/user", "models/post", "models/activity", "collections/post", "morris"], function (React, Navbar, SimplePostComponent, Post, Feed, UserActivity, User, PostModel, Activity, PostCollection, Morris) {

  var root = "http://localhost:1337";

  var Profile = React.createClass({
    setActive: function (e) {
      this.setState({activeTab: e.target.innerHTML});
    },

    getDefaultProps: function () {
      return {
        activeTabPane: "tab-pane active"
      }
    },
    getInitialState: function () {
      return {
        postItems: [],
        headlines: [],
        postText: '',
        viewPost: "all",
        user: {},
        activeTab: 'Activity',
        followers: 0
      }
    },
    expandStatusBox: function (e) {
      e.target.setAttribute("rows", "3");
    },
    handlePostChange: function (e) {
      this.setState({postText: e.target.value});
    },
    sendPost: function () {
      console.log("sendposts called")
      var postModel = new PostModel();
      var destinationId = null;
      if (this.props.param_id && this.props.param_id != this.props.user_id && document.URL == root + '/#/profile/' + String(this.props.param_id)) {

        destinationId = this.props.param_id
      }

      postModel.save(
        {
          initiator: this.props.user_id,
          postType: 'Post',
          content: this.state.postText,
          postDestinationOwner: destinationId
        },
        {
          url: '/post',
          success: function (model, response, options) {
            this.state.postItems.unshift(response);
            this.setState({postItmes: this.state.postItems, postText: ''});

          }.bind(this),
          error: function (resp) {
            /* POST COULD NOT BE UPDATED */
          }
        }
      )
    },

    followUser: function () {
      var follow = new Activity();
      var followDetails = {
        initiator: String(this.props.user_id),
        parentId: String(this.props.param_id),
        parentCollection: 'User',
        activityType: 'Follow'
      };
      follow.save(followDetails, {
        success: function (follow) {
        }
      })


      var val = $('#followButton').text();
      console.log("val", val)
      if (val == 'Follow') {
        this.setState({ followers: this.state.followers + 1 })
      } else {
        this.setState({ followers: this.state.followers - 1 })
      }
      $('#followButton').toggleClass('follow').text($('#followButton').hasClass('follow') ? val = "Following" : val = "Follow");
      // $('#followButtonAlien').toggleClass('follow').text( $('#followButtonAlien').hasClass('follow') ? val="Following": val="Follow"  );


      document.getElementById('followButton').innerHTML = val;
      //document.getElementById('followButtonAlien').innerHTML=val;
    },

    getProfiles: function (id, cb) {
      var user = new User();
      user.set({id: id});
      user.fetch({
        success: function (response) {
          cb(response);
        }
      })

    },

    getProfile: function (id) {
      var user = new User();
      user.set({id: id});
      this.setState({user: user});
      var that = this;
      user.fetch({
        success: function (response, user) {
          that.setState({user: response});
          that.forceUpdate();
        }
      })
      /* User data fetched. */
    },
    getPosts: function (id) {
      /* Fetch posts relating to this user */
      console.log('got called')
      var postModel = new PostCollection();
      this.setState({postItems: postModel});
      var postings = postModel.fetchCurrent(id, function (posts) {
          this.setState({postItems: posts});
        }.bind(this)
      );
      /* Posts fetched */
    },

    message: function () {
      var x;
      var message = prompt("Please enter your message");
      if (message != null) {
        console.log(message);
      }
    },

    componentWillReceiveProps: function (nextProps) {
      //temporary solution, will be fixing this soon enough
      if (nextProps.param_id && document.URL == root + '/#/profile/' + String(nextProps.param_id)) {
        this.getProfile(nextProps.param_id);
        console.log('yay for props paramas')
        this.getPosts(nextProps.param_id);

      }

      else {
        this.getProfile(nextProps.user_id);
        console.log('yay for props user')

        this.getPosts(nextProps.user_id);
      }
    },

    componentWillMount: function () {
      //temporary solution, will be fixing this soon enough
      if (this.props.param_id && document.URL == root + '/#/profile/' + String(this.props.param_id)) {
        this.getProfile(this.props.param_id);
        console.log('yay for comp paramas')

        this.getPosts(this.props.param_id);
      }
      else {
        this.getProfile(this.props.user_id);
        console.log('yay for comp user')

        this.getPosts(this.props.user_id);
      }
    },

    componentDidMount: function () {
      /*  $('ul.nav a').on('shown.bs.tab', function (e) {
       var types = $(this).attr("data-identifier");
       var typesArray = types.split(",");
       $.each(typesArray, function (key, value) {
       eval(value + ".redraw()");
       })
       });
       $(document).ready(function(){
       Morris.Donut({
       element: 'donut-example',
       data: [
       {label: "Download Sales", value: 12},
       {label: "In-Store Sales", value: 30},
       {label: "Mail-Order Sales", value: 20}
       ]
       });

       })*/
    },

    render: function () {

      var that = this;
      var user = this.state.user;
      /* All further references to this.state.user will use the user variable */
      var tagLength = user.get('tags').length;
      var tags = user.get('tags').map(function (tag) {
        tagLength--;
        return tagLength > 0 ? (tag.text + ', ') : tag.text;
      })
      var titleLength = user.get('pro_title').length;
      var title = user.get('pro_title').map(function (title) {
        titleLength--;
        var str = '';
        if (titleLength > 0)
          str = title.company ? (title.text + ' @ ' + title.company + ', ') : (title.text + ', ')
        else
          str = title.company ? (title.text + ' @ ' + title.company) : (title.text)
        return str;
      })
      var accredited = {
        "hide": !user.get('accredited'),
        "text-center": true,
        "label": true,
        "label-success": true,
        "pull-right": true
      };
      var lilink = {
        "hide": user.get('profile_linkedIn'),
        "pull-left": true,
        "grey": true
      }
      var fblink = {
        "hide": user.get('profile_facebook'),
        "pull-left": true,
        "grey": true
      }
      var twlink = {
        "hide": user.get('profile_twitter'),
        "pull-left": true,
        "grey": true
      }

      var noOfInvestments = user.get('investments').length;
      var followers = user.get('followers').length;
      var following = user.get('following').length;
      var badges = user.get('badges').map(function (badge) {
        var classObj = {
          "fa": true,
          "fa-fw": true
        };

        switch (badge.id) {
          case '0':
            classObj["fa-briefcase"] = true;
            break;
          case '1':
            classObj["fa-shopping-cart"] = true;
            break;
        }

        return (
          <h3 key={badge.id}>
            <span className={React.addons.classSet(classObj)}></span>
            <small>{badge.text}</small>
          </h3>
          );
      });
      console.log("param", this.props.param_id)
      console.log("user", this.props.user_id)

      var Posts = this.state.postItems.map(function (post) {
        /*
         if(post.get('postDestinationOwner')){
         that.getProfiles(post.get('initiator'),function(diff){
         return <SimplePostComponent postData={post} userData={diff} key={post.get('id')} />
         })
         }
         else{
         return <SimplePostComponent postData={post} userData={user} key={post.get('id')} />
         }
         */
        return <SimplePostComponent postData={post} userData={user} key={post.get('id')} />
      });

      return (
        <div>
          <Navbar sendIdToParent={this.props.sendIdToParent} userId={this.props.user_id}/>
          <div className="row-fluid">
            <div className="col-md-offset-1 col-md-6">
              <div className="row well well-sm">


              </div>
                            {/*<div className="row well well-sm">
                             <div className="col-md-2">
                             <img src={ user.get('imageLink') } className="profileImage img-responsive" alt="Responsive image"/>
                             </div>
                             <div className="col-md-7">
                             <h3>{user.get('name')}<span className={React.addons.classSet(accredited)}><span className="fa fa-check-square-o marginr10"></span>Accredited</span></h3>
                             <p>{user.get('profileTitle')}</p>
                             <hr />
                             <p>{user.get('city')}, {user.get('country')}<span className="pull-right">{tags}</span></p>
                             <p className="pull-right">
                             <a href={user.get('profileLinkedIn')} target="_blank" className={React.addons.classSet(lilink)}><span className="fa fa-linkedin fa-lg marginr10"></span></a>
                             <a href={user.get('profileFacebook')} target="_blank" className={React.addons.classSet(fblink)}><span className="fa fa-facebook fa-lg marginr10"></span></a>
                             <a href={user.get('profileTwitter')} target="_blank" className={React.addons.classSet(twlink)}><span className="fa fa-twitter fa-lg marginr10"></span></a>
                             {/*this.props.isVisitor ? (user.get('followers').indexOf(this.props.user_id) != -1 ? <a className="btn btn-success marginr10">Following</a> : <a className="btn btn-default marginr10" onClick={this.followUser}>Follow</a>) : <a></a>}
                             {this.props.isVisitor ? <a className="btn btn-primary marginr10">Message</a> : <a className="btn btn-primary marginr10">Dashboard</a>}
                             <span className="label label-warning pull-right">{followers} followers</span>*}
            </p>
          </div>
          <div className="col-md-3">
            <div className="pull-right">
                                        {badges}
            </div>
            <h3>{noOfInvestments} Investments</h3>
            <br />
            <h4>
              <span className="fa fa-coffee marginr10"></span>
            Contact
              <b className="caret"></b>
            </h4>
            <address>
              <strong>{user.get('org_name')}</strong>
              <br/>
                                        {user.get('address')}
              <br/>
                                        {user.get('city')}, {user.get('state')}
              <br/>
              <abbr title="Phone">P:</abbr> {user.get('phone')}
            </address>
          </div>

          <div className="container-fluid">
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul className="nav navbar-nav pull-left" style={{"font-size": "90%"}}>
                <li className="active">
                  <a href="#" style={{"padding": "7px", "padding-right": "15px"}}>Posts
                    <br/> {user.get('noOfPosts')}</a>
                </li>
                <li>
                  <a href="#" style={{"padding": "7px", "padding-right": "15px"}}>Following
                    <br/>{following}</a>
                </li>
                <li>
                  <a href="#" style={{"padding": "7px", "padding-right": "15px"}}>Followers
                    <br/> {this.state.followers}</a>
                </li>
              </ul>

              <ul className="nav navbar-nav navbar-right">
                                            { this.props.param_id && this.props.user_id != this.props.param_id ? <li>{user.get('followers').indexOf(this.props.user_id) != -1 ?
                                              <button id="followButtonAlien"  onClick={this.followUser} className="btn btn-success marginr10">Following</button> : <button id="followButton" className="btn btn-default" style={{"margin-right": "80px"}}
                                            onClick={this.followUser}>Follow</button>}</li> : <li></li>}
                                 
                                            { this.props.param_id && this.props.user_id != this.props.param_id ? <li>
                                              <button className="btn btn-primary" onClick={this.message}>Message</button>
                                            </li> : <li></li>}
              </ul>
            </div>
          </div>

        </div> */
    }
  </div>
    <div className="row">
      <div className="col-md-offset-1 col-md-3">
        <div className="row">

          <ul className="nav nav-tabs col-md-12" >
            <li className={ this.state.activeTab == 'Portfolio' ? "active" : "" }>
              <a className="pointer" onClick={this.setActive} >Portfolio</a>
            </li>
            <li className={ this.state.activeTab == 'Activity' ? "active" : "" }>
              <a className="pointer" onClick={this.setActive} >Activity</a>
            </li>

          </ul>

          <div className="tab-content">
            <div className={ this.state.activeTab == 'Portfolio' ? this.props.activeTabPane : "tab-pane" } >
                                                    {/*<h3>Chart</h3>
                                                     <div id="donut-example" style={{"height": "200px"}}></div>*/}
                                                   {/* <UserActivity user={user} id={this.props.user_id}/>*/}
              <div className="row well well-sm">

                <Feed id ={this.props.user_id}/>
              </div>

            </div>
          </div>
          <div className="tab-content well well-sm">
            <div className={ this.state.activeTab == 'Activity' ? this.props.activeTabPane : "tab-pane" } >
              <div className="row ">
                <div className="col-md-12 margint10 marginb20">
                  <div className="col-md-2">
                    <img src={ user.get('imageLink') } className="profileImage img-responsive" alt="Responsive image"/>
                  </div>
                  <div className="col-md-8">
                    <textarea className="form-control" placeholder="Share an Update" rows="1" value={this.state.postText} onClick={this.expandStatusBox} onChange={this.handlePostChange}></textarea>
                  </div>
                </div>
                <div className="col-md-2">
                  <button className="btn btn-primary" onClick={this.sendPost}>Post Update</button>
                </div>
              </div>
                                                {Posts}
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>

  </div >
  )
}
})

return Profile;

})