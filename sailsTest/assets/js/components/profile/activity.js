define(["react", "models/user", "models/activity", "models/post", "collections/post"], function(React, User, Activity, PostModel, PostCollection){
	
	var UserActivity=React.createClass({

	getInitialState: function(){
		return{
			postItems:[],
		}
	},

	fetchposts: function(id){

	

      var postModel = new PostCollection();
        this.setState({postItems: postModel});
      postModel.fetchCurrent(id, function(posts){
            this.setState({postItems: posts});
        }.bind(this))
    },

    componentWillMount: function(){
    	this.fetchposts(this.props.id);
    },

    componentWillReceiveProps: function(nextProps){
    	this.fetchposts(nextProps.id);
    }, 


	render: function (){

		var user= this.props.user;
		var activity=new Activity();
		var activityType=activity.get('activityType');
		var activityText='';
		switch (activityType){
			case 'invest': activityText="invetsted in ";break;
			case 'like': activityText="liked ";break;
			case 'follow': activityText="followed ";break;
			case 'share': activityText="shared ";break;
		}

		var posts = this.state.postItems.map(function (post) {
			

    //      return post.featured ? <FeaturedPost data={post} key={post.id} /> : <Post data={post} key={post.id} />;
	 return (


			<div className="row row-fluid" >
				<div className=" col-md-12" style={{"margin-top":"20px"}}>
					<div className="col-md-2">
						<img src={ user.get('imageLink') } 
						className="img-responsive" alt="Responsive image"  style={{"width":"100%", "height":"100%"}}/>
					</div>
					<div className="col-md-10">
						<div className="col-md-10">
							<h4>{user.get('name')} added a new <strong> Post</strong></h4>
						</div>
						<div className="pull-right col-md-2">
							<small> time </small>
						</div>
					</div>
					<div className="" >
						
						<div className="col-md-9">
							<h4> {post.get('content')}</h4>
						</div><br/>
						<div className="col-md-offset-2">
							<p style={{"color":"gray"}}> xyz and 5 others like this</p>
						</div>
					</div>
				</div><hr/>
			</div>

		)
        }.bind(this))

			return (

			<div>
			{posts}
			</div>
)
	}
	})

	return UserActivity;
})