
define(["react", 'jsx!components/explore/fundDetails', 'jsx!components/explore/fundFilters'], function (React, FundDetails, FundFilters) {
  var News = React.createClass({
    getInitialState: function () {
      return {
        newsItems: [],
        activeState: 'fundFilter',
        fundId: ''
      }
    },
    showFundDetails : function (fundId){
      console.log(fundId);
      this.setState({'activeState': 'fundDetails', 'fundId': fundId});
    },
    render: function () {
      var activeSection;

      switch (this.state.activeState) {
        case 'fundFilter':
          activeSection = <FundFilters userId = {this.props.userId} showFundDetails={this.showFundDetails}/>;
          break;
        case 'fundDetails':
          activeSection = <FundDetails userId = {this.props.userId} fundId={this.state.fundId}/>;
          break;
        default:
          activeSection = <FundFilters userId = {this.props.userId} showFundDetails={this.showFundDetails}/>;
          break;
      }

      return (
        <div>
        {activeSection}
        </div>
        )
    }
  });

  return News;
});
