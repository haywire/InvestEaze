define(["react", 'rangeSlider', 'starrating', "models/activity"], function (React, Slider, StarRating, Activity) {
  var FilterItem = React.createClass({
    getInitialState: function () {
      return {
        newsItems: []
      }
    },
    followUser: function (fundId) {
      console.log(fundId);
      var follow = new Activity();
      var followDetails = {
        initiator: String(this.props.userId),
        parentId: String(this.props.fundId),
        parentCollection: 'User',
        activityType: 'Follow'
      };
      follow.save(followDetails, {
        success: function (follow) {
        }
      });

      var followBtn = $('#follow-'+ fundId);
      followBtn.toggleClass('btn-danger').text(followBtn.hasClass('btn-danger') ? 'Unfollow' : 'Follow');
    },
    componentDidMount: function (){
      var that = this;
      $("#starRating-" + this.props.item.fundId).jRating({
        type:'small', // type of the rate.. can be set to 'small' or 'big'
        length : 5, // nb of stars
        decimalLength : 5,
        showRateInfo:false, // number of decimal in the rate
        isDisabled: true
      });
    },
    render: function () {
      var that = this;
      return (
        <div>
            <div className='col-md-12' style={{'font-size': '10px', 'margin-top': '14px', 'border-bottom': '1px solid lightgray',
              'padding-bottom': '10px'}}>
              <div className='col-md-2' style={{'border-right': '1px solid lightgray', 'height': '90px', 'width': '115px', 'margin-left':'-25px'}}>
                <div><img src='/images/logo-fund.png' style={{'float': 'left', 'height': '33px'}}/></div>
                <div><a href='javascript:void(0);' onClick = {this.props.showFundDetails.bind(null, this.props.item.fundId)} className='filter-fund-name'>{this.props.item.fundName}</a></div>
              </div>
              <div className='col-md-2' style={{'border-right': '1px solid lightgray', 'height': '90px'}}>
                <div className='heading'> Crisil Rating</div>
                <div style={{'text-align': 'center'}}>
                Rank {this.props.item.fundRank}
                  <div id={'starRating-'+ this.props.item.fundId} data-average={this.props.item.fundRating}></div>
                </div>
              </div>
              <div className='col-md-2' style={{'border-right': '1px solid lightgray', 'height': '90px'}}>
                <div className='heading'> AUM</div>
                <div style={{'text-align': 'center'}}>
                Rs. {this.props.item.aum}
                </div>
              </div>
              <div className='col-md-2' style={{'border-right': '1px solid lightgray', 'height': '90px'}}>
                <div className='heading'> NAV</div>
                <div style={{'text-align': 'center'}}>
                Rs. {this.props.item.nav}
                </div>

              </div>
              <div className='col-md-3' style={{'border-right': '1px solid lightgray', 'height': '90px'}}>
                <div className='heading'> Performance</div>
                <div style={{'text-align': 'center'}}>
                  <div style={{'float': 'left'}}>
                    <span style={{'color': 'green', 'font-size': '16px'}}> {this.props.item.performance.quarter} % </span>
                    <br/>
                    <small> 90 days </small>
                  </div>
                  <div>
                    <span style={{'color': 'green', 'font-size': '16px'}}>{this.props.item.performance.yearly} % </span>
                    <br/>
                    <small> 365 days</small> <br/> <small style={{'float': 'right', 'height': '90px'}}> {this.props.item.performance.since} </small>
                  </div>
                </div>
              </div>
              <div className='col-md-1'>
              {(function(){
                if(that.props.item.followers.indexOf(that.props.userId) == -1){
                  return <button id={'follow-' + that.props.item.fundId} className="btn btn-success btn-xs followButton" onClick={that.followUser.bind(null, that.props.item.fundId)} style={{'margin-top': '20px'}}> Follow </button>;
                } else {
                  return <button id={'follow-' + that.props.item.fundId} className="btn btn-danger btn-xs followButton follow" onClick={that.followUser.bind(null, that.props.item.fundId)} style={{'margin-top': '20px'}}> Unfollow </button>;
                }
                })()}
              </div>
            </div>
        </div>
        )
    }
  });

  return FilterItem;
});