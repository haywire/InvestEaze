define(["backbone", "models/fund"], function (Backbone, Fund) {

  var FundCollection = Backbone.Collection.extend({
    model: Fund,

    url: function () {
      return '/fund/' + this.url_append;
    },

    fetchFundDetails: function (id, callback) {
      this.url_append = 'details/' + String(id);
      this.fetch({
        success: function (obj) {
          callback(obj);
        }
      });
    }
  });
  return FundCollection;
});