define(["backbone", "models/notification"], function(Backbone, NotificationModel){
	var NotificationCollection = Backbone.Collection.extend({
		model: NotificationModel,
		url: function() {
			return '/notification/' + this.url_append;
		},
		fetchList: function(id, callback) {
			this.url_append = 'list/' + String(id);
			this.fetch({
				success: function(child, response){
					console.log("Fetched Notifications Successfully:- ", response);
					callback(response);
				},
				error: function(child, error){
					console.log("Error Getting Notifications:- ", error);
					callback(error);
				}
			});
		}
	});

	return NotificationCollection;
});