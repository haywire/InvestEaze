define(["backbone", "models/comment"], function(Backbone, CommentModel) {

	var Comment = Backbone.Collection.extend({
		model: CommentModel,
		
        url: function () {
            return '/comment' + this.url_append;
          },

        fetchCurrent: function (id, callback) {
            
            this.url_append = "/fetch/" + String(id);
                 this.fetch({
                success: function(obj) {
                    callback(obj);
                }
            });
            
        }

	})

	return Comment;

})