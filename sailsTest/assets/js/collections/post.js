define(["backbone", "models/post"], function(Backbone, PostModel) {

	var Post = Backbone.Collection.extend({
		model: PostModel,

	    url: function () {
            return '/post' + this.url_append;
          },

        fetchCurrent: function (id, callback) {
            
            this.url_append = "/list/" + String(id);
                 this.fetch({
                success: function(obj) {
                    callback(obj);
                }
            });
            
        }

	})

	return Post;

})